// **********************************************************
// Assignment2:
// Student1: Quoc Lam Ta
// UTOR user_name: taquoc
// UT Student #: 1001644740
// Author: Quoc Lam Ta
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
// *********************************************************

package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import Commands.*;
import driver.*;
import Helper.*;
/**
 * 
 * @author Spasimir
 *
 */
public class TestMkDir {
  Commands mkdir;
  Commands ls;
  CD cd;
  Commands pwd;
  PathHandler path;
  FileSystem FS;
  String [] args;
  String executer;

  @Before
  public void setUp(){
    mkdir = new MkDir("");
    path = new PathHandler();
    FS = new FileSystem("root");
    ls = new LS("");
    pwd = new PWD("");
  }

  @Test
  /*
   * test if mkdir makes a new directory 
   */
  public void testExecute() throws Exception {
    args = new String[1];
    args[0] = "Folder";
    mkdir.setArgument(args);
    mkdir.execute(FS);
    ls.setArgument(new String[0]);
    executer = ls.execute(FS);
    assertEquals("Folder\t", executer);
  }
  
  @Test
  /*
   * test if mkdir makes multiple new directories
   */
  public void testExecuteWithMultipleDirectories() throws Exception{
    args = new String[3];
    args[0] = "Folder";
    args[1] = "Folder2";
    args[2] = "Folder3";
    mkdir.setArgument(args);
    mkdir.execute(FS);
    ls.setArgument(new String[0]);
    executer = ls.execute(FS);
    assertEquals("Folder3\tFolder2\tFolder\t", executer);
  }
  
  @Test
  /*
   * test if mkdir gives the proper error when you're trying to make a directory
   * which alrady exists
   */
  public void testExecuteWithAnExistingDirectory() throws Exception{
    args = new String[1];
    args[0] = "Folder";
    mkdir.setArgument(args);
    mkdir.execute(FS);
    executer = mkdir.execute(FS);
    assertEquals("Dir Folder is invalid", executer);
  }
  
  @Test
  /*
   * test if mkdir returns the proper error when . or .. is typed as a name
   */
  public void testExecuteWithDotAndDoubleDot() throws Exception{
    args = new String[1];
    args[0] = ".";
    mkdir.setArgument(args);
    executer = mkdir.execute(FS);
    assertEquals("error", executer);
    
    args[0] = "..";
    mkdir.setArgument(args);
    executer = mkdir.execute(FS);
    assertEquals("error", executer);
  }
  
  @Test
  /*
   * test if mkdir created a directory with a specified path
   */
  public void testExecuteWithPath() throws Exception{
    args = new String[1];
    args[0] = "Folder";
    mkdir.setArgument(args);
    mkdir.execute(FS);
    
    args[0] = "Folder/Folder2";
    mkdir.setArgument(args);
    mkdir.execute(FS);
    
    cd = new CD("cd Folder/Folder2");
    FS = cd.changPath(FS);
    executer = pwd.execute(FS);
    assertEquals("/Folder/Folder2", executer);
  }

}
