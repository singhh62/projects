// **********************************************************
// Assignment2:
// Student1: Quoc Lam Ta
// UTOR user_name: taquoc
// UT Student #: 1001644740
// Author: Quoc Lam Ta
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//

package Helper;

import java.util.*;
import driver.*;

/**
 * @author Quoc Lam Ta
 *
 */
public class PathHandler {

  public PathHandler() { // Declare class

  }

  /**
   * Redirect the current working file system to new path
   * 
   * @param fs the current working FileSystem
   * @param path the new paths to direct fs to
   * @param isFull is an absolute path
   * @return the new path from paths and Null if a path does not exist
   */
  public FileSystem toThePath(FileSystem fs, String[] path, boolean isFull) {
    if (isFull) {
      return handlePath(fs, path, true); // go to helper function
    } else
      return handlePath(fs, path, false); // go to helper function
  }

  /**
   * handle the path by conditional recursion
   * 
   * @param fs current working FileSystem
   * @param path the path we want to go to
   * @param isFull is an absolute path
   * @return FileSystem that we want to go to
   */
  private FileSystem handlePath(FileSystem fs, String[] path, boolean isFull) {
    if (fs.getParent() == null && isFull) { // fs is at root
      List<String> listPaths = new ArrayList<String>();
      for (String pathName : path)
        if (!pathName.equals(""))
          // add the path names into an array
          listPaths.add(pathName);
      String[] newPaths = new String[listPaths.size()];
      // recursive call with a smaller array path
      return handlePath(fs, listPaths.toArray(newPaths), false);
    } else if (fs.getParent() != null && isFull) { // fs is not at the root
      // recursive call with the parent of fs until fs is at root
      return handlePath(fs.getParent(), path, true);
    } else if (path.length == 0) {
      // when the array is empty return the File System
      return fs;
    } else if (path[0].equals("..") && fs.getParent() != null) {
      // go to the parent of fs with ".."
      return handlePath(fs.getParent(),
          Arrays.copyOfRange(path, 1, path.length), false);
    } else if (path[0].equals(".")) {
      // keep current fs and make a recursive call for smaller array
      return handlePath(fs, Arrays.copyOfRange(path, 1, path.length), false);
    } else if (fs.getChildrenFS().containsKey(path[0])) {
      return handlePath(fs.getChildrenFS().get(path[0]),
          Arrays.copyOfRange(path, 1, path.length), false);
    } else { // when path does not exist return null
      return null;
    }
  }
}
