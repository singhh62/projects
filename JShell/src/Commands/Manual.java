// **********************************************************
// Assignment2:
// Student1: Quoc Lam Ta
// UTOR user_name: taquoc
// UT Student #: 1001644740
// Author: Quoc Lam Ta
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//

package Commands;

import java.util.*;
import driver.*;
import Helper.*;

/**
 * @author Spasmir
 *
 */
public class Manual extends Commands {
  private Hashtable<String, String> commands = mapping();

  public Manual(String userInput) {
    super.setInput(userInput);
  }

  /**
   * According to the command, return the corresponding description
   * 
   * @param fs fileSystem object
   * @throws Exception
   * @return the description of the manual
   */
  public String execute(FileSystem fs) throws Exception {
    if (commands.containsKey(getArgumentAtPosition(0))
        && getArgumentAtPosition(0) != "others") {
      // Gets the description of the command
      return commands.get(getArgumentAtPosition(0)).trim();
    } else {
      throw new Exception(getArgumentAtPosition(0) + " is invalid, try again!");
    }
  }

  /**
   * Add content to the hashtable, command as key and its description as value
   * 
   * @return The hashtable of the commands and description
   */
  private Hashtable<String, String> mapping() {
    Hashtable<String, String> commands = new Hashtable<String, String>();
    commands.put("mkdir", "mkdir - Create Directory(ies)");
    commands.put("cd", "cd - Change the working directory");
    commands.put("ls", "ls - Print the content of the working directory");
    commands.put("pwd", "pwd - Print the path of the working directory");
    commands.put("pushd", "pushd - Save and than change the current directory");
    commands.put("popd",
        "popd - Remove the top directory from the stack and cd into it");
    commands.put("history", "history - Print of the commands recently used");
    commands.put("cat", "cat - Display the content of the file");
    commands.put("echo", "echo - Display a line of text or put it in a file");
    commands.put("cat", "cat - Print the documentation of a command");
    commands.put("mv", "mv - Move item OLDPATH to NEWPATH");
    commands.put("cp", "cp - Copy item from OLDPATH to NEWPATH");
    commands.put("get", "get - Retrieve the file at that URL and add it to"
        + " the current working directory");
    commands.put("grep", "grep - print any line that contains the REGEX");
    commands.put("!number",
        "!number - recall any of " + "previous history by its number");
    commands.put("man", "man - Print the manual of the commands");
    return commands;
  }
}
