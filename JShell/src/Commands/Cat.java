// **********************************************************
// Assignment2:
// Student1: Quoc Lam Ta
// UTOR user_name: taquoc
// UT Student #: 1001644740
// Author: Quoc Lam Ta
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//

package Commands;

import java.util.Arrays;
import driver.*;
import Helper.*;

/**
 * @author Raj
 *
 */
public class Cat extends Commands {
  /**
   * Display the content of file
   *
   * @param input user input of the file name
   */
  public Cat(String userInput) {
    super.setInput(userInput); // super is the Command class
  }

  /**
   * Return the content of the file in the form of a String
   *
   * @param fs file system object
   * @return a string of the content of the file
   * @throws Exception
   */
  public String execute(FileSystem fs) throws Exception {
    String result = "";
    for (String argument : getArguments()) { // Each argument
      try {
        result += contentGrabber(fs, argument) + "\n";
      } catch (Exception e) {
        System.out.println(argument + " does not exist");
      }
    }
    return result;
  }

  /**
   * Get the content of the file in the form of a String
   *
   * @param fs file system object
   * @param argument all the file names passed
   * @return a string of the content of the file
   * @throws Exception
   */
  private String contentGrabber(FileSystem fs, String argument)
      throws Exception {
    String[] paths = argument.split("/"); // Split at the /, so it gets all the
                                          // paths
    String fileName = paths[paths.length - 1];
    FileSystem fsClone = getPathHandler().toThePath(fs,
        Arrays.copyOfRange(paths, 0, paths.length - 1),
        argument.startsWith("/"));
    if (fsClone.getChildrenFile().containsKey(fileName)) {
      return "File \"" + fileName + "\" contains: \n" // The output
          + fsClone.getChildrenFile().get(fileName).getContent();
    } else {
      // If the file dosen't exists
      throw new Exception("File \"" + fileName + "\" does not exist");
    }
  }
}
