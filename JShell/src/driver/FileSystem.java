// **********************************************************
// Assignment2:
// Student1: Hardeep Singh
// UTOR user_name: sinhh62
// UT Student #: 999741793
// Author: Quoc Lam Ta
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//

package driver;

import java.util.*;
import Commands.*;
import Helper.*;

import Helper.PathHandler;

/**
 * @author Hardeep Singh
 *
 */
public class FileSystem {

  /**
   * parent of the FileSystem
   */
  private FileSystem parent;
  private String name;
  private Hashtable<String, FileSystem> childrenFS; // HashTable for children
  private Hashtable<String, File> childrenFile; // HashT for the children File


  /**
   * return the parent of the this
   * 
   * @return parent of the FileSystem
   */
  public FileSystem getParent() {
    return parent;
  }

  /**
   * set the parent of the this
   * 
   */
  public void setParent(FileSystem parent) {
    this.parent = parent;
  }



  /**
   * constructor
   */
  public FileSystem(String dirName) {
    name = dirName;
    childrenFS = new Hashtable<String, FileSystem>();
    childrenFile = new Hashtable<String, File>();
  }

  /**
   * Get the name of the FS
   * 
   * @return the name of the FS, that was passed through constructor
   */
  public String getName() {
    return name; // get name of FileSystem
  }

  /**
   * Set a new name
   * 
   * @param dirName the directory name
   */
  public void setName(String dirName) {
    name = dirName; // set name of FileSystem
  }

  /**
   * Get the content of a directory, for example used for LS
   * 
   * @return the string that contains the directories within a directory
   */
  public String getContent() {
    String fileContent = "";
    for (FileSystem folder : getChildrenFS().values()) { // Go through the
                                                         // values
      fileContent += folder.getName() + "\t".replaceAll("\\s+", "\t");
    }
    for (File file : getChildrenFile().values()) {
      fileContent += file.getName() + "\t".replaceAll("\\s+", "\t");
    }
    return fileContent;
  }

  /**
   * Add a directory to the file system tree
   * 
   * @param dirName the directory name
   */
  public void addFileSystem(String dirName) {
    FileSystem temp = new FileSystem(dirName);
    temp.parent = this;
    childrenFS.put(dirName, temp); // Put the directory in ChildrenFS hashtable
  }

  /**
   * Add a file to to the file system
   * 
   * @param fileName name of the file
   * @param isOverWritten if it is going to be overWritten
   * @param fileContent content of the file
   */
  public void addFile(String fileName, boolean isOverWritten,
      String fileContent) {
    File temp = new File(fileName);
    temp.addContent(isOverWritten, fileContent); // Adds file
    childrenFile.put(fileName, temp); // Puts it in the childrenFile HashTable
  }

  /**
   * return the Hashtable of childrenFile
   * 
   * @return the HashTble of childrenFile
   */
  public Hashtable<String, File> getChildrenFile() {
    return childrenFile; // get childrenFile
  }

  /**
   * Returns the Hashtable for childrenFs
   * 
   * @return a Hashtable of childrenfs
   */
  public Hashtable<String, FileSystem> getChildrenFS() {
    return childrenFS; // get childrenFS
  }

  /**
   * Check the entered path exists
   * 
   * @param fs FileSystem Object
   * @param path user inputed path
   * @param toLastPath to the path before
   * @param pathHandler pathHandler object
   * @throws Exception 
   */
  public boolean isPathExist(FileSystem fs, String path, boolean toLastPath,
      PathHandler pathHandler) throws Exception {
    int siseOfNewPath;
    if (toLastPath) {
      siseOfNewPath = path.split("/").length; // Split the string at /
    } else {
      // Do not check the last path name
      siseOfNewPath = path.split("/").length - 1;
      if (siseOfNewPath < 0) {
        siseOfNewPath = 0;
      }
    }
    String[] paths = Arrays.copyOfRange(path.split("/"), 0, siseOfNewPath);
    if (pathHandler.toThePath(fs, paths, path.startsWith("/")) != null) {
      return true; // return true if such path exists
    }
    return false; // else return false
  }

  /**
   * return all name of directories and files of current FileSystem
   * 
   * @return all name of children of current working directory
   */
  public ArrayList<String> getAllChildrenName() {
    ArrayList<String> childrenName = new ArrayList<String>();
    // add name from childrenFile
    childrenName.addAll(this.getChildrenFile().keySet());
    childrenName.addAll(getChildrenFS().keySet()); // add name from childrenFS
    return childrenName;
  }

  /**
   * check if the directory exists in the current working directory
   * 
   * @param dirname the directory's name
   * @return true iff FileSystem contains dirName
   */
  public boolean isFSExist(String dirName) {
    return this.getChildrenFS().containsKey(dirName);
  }

  /**
   * check if the file exists in the current working directory
   * 
   * @param dirname the file's name
   * @return true iff FileSystem childrenFile contains fileName
   */
  public boolean isFileExist(String fileName) {
    return this.getChildrenFile().containsKey(fileName);
  }

  /**
   * Check if the current directory has any child with that name
   * 
   * @param dirname the directory's name
   * @return true iff FileSystem's children contains name
   */
  public boolean isChildrenExist(String name) {
    return this.getAllChildrenName().contains(name);
  }
  /**
   * delete a file by its name in current working directory
   * 
   * @param fileName the name of the file
   */
  public void delFile(String fileName) {
    this.childrenFile.remove(fileName);
  }
  
  /**
   * delete a directory by its name in current working directory 
   * 
   * @param fsName the name of the directory
   */
  public void delFs(String fsName) {
    this.childrenFS.remove(fsName);
  }

  /**
   * add another exist directory as a child of current working directory
   * 
   * @param other the child system to be add
   */
  public void setFS(FileSystem other) {
    other.setParent(this);
    this.childrenFS.put(other.getName(), other);
  }
  
  /**
   * add exist content of a directory to another directory
   * 
   * @param files files of another directory
   * @param dirs subdirectories of another directory 
   */
  public void setFSChidlren(Hashtable<String, File> files, Hashtable <String, FileSystem> dirs){
    for (String fileName: files.keySet()){
      this.addFile(fileName, true, files.get(fileName).getContent());
    }
    for(String dirName: dirs.keySet()){
      this.setFS(dirs.get(dirName));;
    }
  }
}
