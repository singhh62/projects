// **********************************************************
// Assignment2:
// Student1: Hardeep Singh
// UTOR user_name: singhh62
// UT Student #: 999741793
// Author: Hardeep Singh
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//

package Helper;
/**
 * 
 * @author Spasimir Vasilev
 *
 */
public class File {

  private String name;
  private String content;

  public File(String fileName) {
    name = fileName;
    content = new String();
  }

  /**
   * Add content to the file. Overwrite existing content
   * 
   * @param isOverwrite True or False if its > or >>
   * @param fileContent the content of the file
   */
  public void addContent(boolean isOverwrite, String fileContent) {
    if (isOverwrite) {
      content = fileContent;
    } else {
      content += "\n" + fileContent;
    }
  }

  /**
   * Return the content of the file
   * 
   * @return the contents of the file
   */
  public String getContent() {
    return new String(content);
  }

  /**
   * Return the name of the file
   * 
   * @return the name of the file
   */
  public String getName() {
    return new String(name);
  }
  
  public void setName(String fileName){
    name = fileName;
  }

}
