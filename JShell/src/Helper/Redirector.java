// **********************************************************
// Assignment2:
// Student1: Hardeep Singh
// UTOR user_name: singhh62
// UT Student #: 999741793
// Author: Hardeep Singh
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//
package Helper;
/**
 * 
 * @author Spasimir
 *
 */
public class Redirector {
  public Redirector(){};
  
  /**
   * Return a String from the beginning of it until a > is encountered
   * @param command is the string for the full command
   * @param argList is the list of arguments for the command
   * @return A String 
   */
  public String getNewCommand(String command, String[] argList) {
    String secondLast;
    String last;
    int stop;
    if (argList.length > 1) {
      secondLast = argList[argList.length - 2];
      last = argList[argList.length - 1];
      // get the last two elements in the argument list
      if (secondLast.equals(">") || secondLast.equals(">>") && last != null) {
        stop = command.indexOf(">") - 1; 
        return command.substring(0, stop); // return a substring
      } else {
        return command; //return the same string
      }
    }
    return command;
  }

  /**
   * Return a boolean value indicating if the string is a redirection
   * @param command is the string for the full command
   * @param argList is the list of arguments for the command
   * @return
   */
  public boolean isRedirect(String command, String[] argList) {
    String secondLast;
    String last;
    if (argList.length > 1) {
      secondLast = argList[argList.length - 2];
      last = argList[argList.length - 1];
      return secondLast.equals(">") || secondLast.equals(">>") && last != null;
    }
    return false;
  }

}
