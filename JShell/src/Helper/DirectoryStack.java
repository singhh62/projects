// **********************************************************
// Assignment2:
// Student1: Hardeep Singh
// UTOR user_name: singhh62
// UT Student #: 999741793
// Author: Hardeep Singh
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//

package Helper;

import java.util.Stack;
import driver.*;
/**
 * 
 * @author Spasimir Vasilev
 *
 */
 public class DirectoryStack { //collaborates with FileSystem
   private Stack<String> listOfDir;
  
  public DirectoryStack(){
    listOfDir = new Stack<String>();
  }
  
  /**
   * Ads a path to the directory
   * this is how command pushd works
   * 
   * @param path user input
   * @param file FileSystem object 
   * @param changePath CD object
   */
  public void add(FileSystem fs){ 
    //changes the working directory to the previous directory
    listOfDir.push(printCurrentWorking(fs, true));
  }
  /**
   * 
   * @param fs
   * @param firstRun
   * @return
   */
  private String printCurrentWorking(FileSystem fs, boolean firstRun) {
	    if (fs.getParent() == null && firstRun == false){
	      return "";
	    }else if (fs.getParent() == null && firstRun == true) {
	      return "/";
	    } else {
	      return printCurrentWorking(fs.getParent(), false) + 
	          "/" + fs.getName();
	    }

	  }
  /**
   * Remove the current directory and goes back to the parent
   * this is how command popd works
   * 
   * @return the current directory
   */
  public String remove(){
    if (isEmpty()){
      System.out.println("Stack is empty");
      return null;
    }
    else{
      return listOfDir.pop();
    }
  }
  /**
   * used for testing only
   * @return the stack
   */
  public Stack<String> getStack(){
    return listOfDir;
  }
  private boolean isEmpty(){
    return listOfDir.empty();
  }
  
  /**
   * Return the current directory without removing
   * @return the parent directory
   */
}
