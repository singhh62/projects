// **********************************************************
// Assignment2:
// Student1: Hardeep Singh
// UTOR user_name: singhh62
// UT Student #: 999741793
// Author: Hardeep Singh
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//
package Helper;

import java.util.Arrays;

import Commands.Echo;
import driver.FileSystem;
/**
 * 
 * @author Spasimir
 *
 */
public class FileCreator {
  private String name;
  private String content;
  private String path;
  private String[] thePath;
  private String[] pathWithoutName;
  private PathHandler pH = new PathHandler();


  public FileCreator(String userInput) {}

  /**
   * OverWrite the file
   *
   * @param fs a file system object
   * @param pathHandler PathHandler object
   * @param arguments is the arguments given
   * @param executer is the content that is going to be written 
   * @throws Exception 
   */
  private void overWriteFile(FileSystem fs, PathHandler pathHandler,
      String[] arguments, String executer) throws Exception {
    String[] paths = arguments[arguments.length - 1].split("/"); // Splits the
                                                                 // string
    String fileName = paths[paths.length - 1]; // Grabs the filename
    FileSystem fsClone = pathHandler.toThePath(fs,
        Arrays.copyOfRange(paths, 0, paths.length - 1), // Creates a FileSystem
        arguments[arguments.length - 1].startsWith("/"));
    // If the FS contains the file
    if (fsClone == null) {
      System.out.println("File is invalid");
      return;
    } else if (fsClone.getChildrenFile().containsKey(fileName)) {
      // Then you want to add the contents
      fsClone.getChildrenFile().get(fileName).addContent(true, executer);
    } else {
      if (!fsClone.getChildrenFS().containsKey(fileName))
        // If not, you want to add the file that not conflicts with FileSystem
        // Name
        fsClone.addFile(fileName, true, executer);
      else {
        System.out.println("The file name \"" + fileName + "\" is invalid");
      }
    }
  }

  /**
   * Append to the file
   *
   * @param fs a file system object
   * @param pathHandler PathHandler object
   * @param arguments is the arguments given
   * @param executer is the content that is going to be written 
   * @throws Exception 
   */
  private void apppendFile(FileSystem fs, PathHandler pathHandler,
      String[] arguments, String executer) throws Exception {
    String[] paths = arguments[arguments.length - 1].split("/"); // Split the
                                                                 // string
    String fileName = paths[paths.length - 1];
    FileSystem fsClone = pathHandler.toThePath(fs,
        Arrays.copyOfRange(paths, 0, paths.length - 1), // Make FileSystem
        arguments[arguments.length - 1].startsWith("/"));
    // If the FileSystem contains the file just append it to the file, if not
    // make the file
    if (fsClone.getAllChildrenName().contains(fileName)) {
      fsClone.getChildrenFile().get(fileName).addContent(false, executer);
    } else {
      fsClone.addFile(fileName, false, executer);
    }
  }

  /**
   * 
   * @param FC is a File System 
   * @param arguments are the arguments given 
   * @param executer is the content that will be used 
   * @throws Exception
   */
  public void execute(FileSystem FC, String[] arguments, String executer)
      throws Exception {

    String secondLast;
    String last;
    PathHandler path = new PathHandler();
    if (arguments.length > 1 && !executer.equals("")) {
      // store the last two arguments 
      secondLast = arguments[arguments.length - 2];
      last = arguments[arguments.length - 1];
      // check if the command given is valid
      if (secondLast.equals(">") || secondLast.equals(">>") && last != null) {
        //overload or overwrite the file
        if (secondLast.equals(">")) {
          overWriteFile(FC, path, arguments, executer);
        } else if (secondLast.equals(">>")) {
          apppendFile(FC, path, arguments, executer);
        }
      }
    }
  }
}
