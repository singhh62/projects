// **********************************************************
// Assignment2:
// Student1: Hardeep Singh
// UTOR user_name: singhh62
// UT Student #: 999741793
// Author: Hardeep Singh
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//

package Helper;

import java.util.Map;
import java.util.HashMap;

/**
 * 
 * @author Hardeep Singh
 *
 */
public class Log {

  private Map<Integer, String> userInputs;
  private int size;

  /**
   * class constructor
   */
  public Log() {
    userInputs = new HashMap<Integer, String>();
    size = 0;
  }

  /**
   * Set new input to be added
   * 
   * @param newInput new user input
   */
  public void setNewInput(String newInput) {
    size++;
    userInputs.put(size, newInput);
  }

  /**
   * Decide how history command should be printed
   *
   * @param numberOfHistory indicates number of inputs will be printed
   * 
   * @return the previous user's input
   * @throws Exception 
   */
  public String printHistory(int numberOfHisroty) throws Exception {
    if (numberOfHisroty == size) {
      return printPreviousInputs();
    } else {
      return printPreviousInputs(numberOfHisroty);
    }
  }

  /**
   * Print inputs that were inputed
   * 
   * @param numberOfInputs number you want the inputs up too
   * @return the string of all the inputs
   * @throws Exception catch when number is below 0
   */
  private String printPreviousInputs(int numberOfHistory) throws Exception{
    String historyOfInputs = "";
    if (numberOfHistory < 0) { // If it is less than 0 return error
      throw new Exception("Enter number greater than 0");
    } else if (numberOfHistory < 1) { // If it is 0, return nothing
      return historyOfInputs;
    } else if (numberOfHistory > size) { // if numberOfHisroty is larger than
                                         // size, return everything
      return printPreviousInputs();
    } // If not go through hashtable and make a big string and return it
    for (int i = (size - numberOfHistory) + 1; i < size + 1; i++) {
      historyOfInputs += i + " " + userInputs.get(i) + "\n";
    }
    return historyOfInputs;
  }

  /**
   * Print all previous inputs
   * 
   * @return the input that was the latest one
   */
  private String printPreviousInputs() {
    String historyOfInputs = "";
    for (int i = 1; i < size + 1; i++) {
      historyOfInputs += i + " " + userInputs.get(i) + "\n";
    }
    return historyOfInputs;
  }

  public String printAtNumber(int position) throws Exception {
    if (0<position && position <= size) {
      return userInputs.get(position);
    } else {
      throw new Exception("invalid length"); // need to add an exception
    }
  }

  public int getSize() {
    return size;
  }
}
