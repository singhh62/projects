// **********************************************************
// Assignment2:
// Student1: Hardeep Singh
// UTOR user_name: singhh62
// UT Student #: 999741793
// Author: Hardeep Singh
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//

package Helper;

import java.util.Arrays;
import driver.*;

/**
 * @author Anees Bajwa
 *
 */

public class Commands {
  private PathHandler pathHandler = new PathHandler();
  private Validate validate = new Validate();
  private String command;
  // user input array
  private String[] arguments;

  public Commands() {

  }

  /**
   * splits input into different arguments
   *
   * @param input User input
   */
  public void setInput(String input) {
    String[] inputSeperator = input.trim().split(" +");
    command = inputSeperator[0];
    arguments = Arrays.copyOfRange(inputSeperator, 1, inputSeperator.length);
  }

  /**
   * Set the arguments to new list of arguments
   * 
   * @param list of arguments
   */
  public void setArgument(String[] arguments) {
    this.arguments = arguments;
  }

  /**
   * set a new argument at a position
   * 
   * @param position, the user inputs
   * @return newArgument the user gives
   * @throws Exception
   */
  public void setArgumentAtPosition(int position, String newArgument) {
    arguments[position] = newArgument;
  }

  /**
   * Set the arguments to new list of arguments
   * 
   * @param new command
   */
  public void setCommand(String command) {
    this.command = command;
  }

  /**
   * Returns command name
   *
   * @return the command portion of the user
   */
  public String getCommand() {
    return command;
  }

  /**
   * Returns a list that contains the arguments of the command
   *
   * @return list of arguments
   */
  public String[] getArguments() {
    return arguments;
  }

  /**
   * Returns a String of the argument at the position
   *
   * @return a string of the argument
   */
  public String getArgumentAtPosition(int position) {
    return arguments[position];
  }

  /**
   * Get the pathHandler
   *
   * @return list of arguments
   */
  public PathHandler getPathHandler() {
    return pathHandler;
  }
  
  public Validate Validate(){
    return validate;   
  }

  /**
   * a method to inherit for polyphormism
   * 
   * @param fs the current FileSystem
   * @return execute needed string
   * @throws Exception
   */
  public String execute(FileSystem fs) throws Exception {
    return "";
  }

  /**
   * an execute method
   * 
   * @param Log log object
   * @return execute needed string
   * @throws Exception
   */
  public String execute(Log fs) throws Exception {
    return "";
  }
}
