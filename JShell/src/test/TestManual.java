// **********************************************************
// Assignment2:
// Student1: Hardeep Singh
// UTOR user_name: singhh62
// UT Student #: 999741793
// Author: Hardeep Singh
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//
package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import Commands.Manual;
import Helper.Commands;
import Helper.PathHandler;
import driver.FileSystem;

/**
 * @author Raj
 *
 */
public class TestManual {
  private Manual man;
  private FileSystem fs;

  @Before
  public void setUp() throws Exception {
    fs = new FileSystem("/");
  }

  @Test
  // Test for mkdir manual
  public void test_mkdir_man() throws Exception {
    man = new Manual("man mkdir");
    assertEquals(man.execute(fs), "mkdir - Create Directory(ies)");
  }

  @Test
  // Test for man manual
  public void test_man_for_man() throws Exception {
    man = new Manual("man man");
    assertEquals(man.execute(fs), "man - Print the manual of the commands");
  }

  @Test
  // Test for cd manual
  public void test_cd_man() throws Exception {
    man = new Manual("man cd");
    assertEquals(man.execute(fs), "cd - Change the working directory");
  }

  @Test
  // Test for cp manual
  public void test_cp_man() throws Exception {
    man = new Manual("man cp");
    assertEquals(man.execute(fs), "cp - Copy item from OLDPATH to NEWPATH");
  }

  @Test
  // Test for mv manual
  public void test_mv_man() throws Exception {
    man = new Manual("man mv");
    assertEquals(man.execute(fs), "mv - Move item OLDPATH to NEWPATH");
  }

  @Test
  // Test for ls manual
  public void test_ls_man() throws Exception {
    man = new Manual("man ls");
    assertEquals(man.execute(fs),
        "ls - Print the content of the working directory");
  }

  @Test
  // Checks if exception thrown
  public void test_error_man() throws Exception {
    ExpectedException exception = ExpectedException.none();
    man = new Manual("man op");
    exception.expectMessage("op is invalid, try again!");
  }
}
