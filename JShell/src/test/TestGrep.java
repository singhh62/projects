// **********************************************************
// Assignment2:
// Student1: Hardeep Singh
// UTOR user_name: singhh62
// UT Student #: 999741793
// Author: Hardeep Singh
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
// *********************************************************
package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Commands.CD;
import Commands.Echo;
import Commands.Grep;
import Helper.Commands;
import Helper.FileCreator;
import driver.FileSystem;
/**
 * 
 * @author Spasimir
 *
 */
public class TestGrep {
  FileSystem system;
  Commands main;
  Commands secondary;
  Commands grep;
  CD cd;
  FileCreator FC;
  String output;
  String grepOutput;
  String expected;


  @Before
  public void setUp() throws Exception {
    system = new FileSystem("");
    FC = new FileCreator("");
    secondary = new Commands();
    main = new Echo("");
  }

  @Test
  public void testOneLine() throws Exception {
    main.setInput("echo string");
    output = main.execute(system);

    secondary.setInput("echo string > file");
    FC.execute(system, secondary.getArguments(), output);

    grep = new Grep("");
    grep.setInput("grep string file");
    grepOutput = grep.execute(system);

    assertEquals("/file: string", grepOutput);
  }

  @Test
  public void testMoreLines() throws Exception {
    main.setInput("echo string is good");
    output = main.execute(system);

    secondary.setInput("echo string is good > file");
    FC.execute(system, secondary.getArguments(), output);

    main.setInput("echo string is great");
    output = main.execute(system);

    secondary.setInput("echo string is great >> file");
    FC.execute(system, secondary.getArguments(), output);

    grep = new Grep("");
    grep.setInput("grep string file");
    grepOutput = grep.execute(system);

    assertEquals("/file: string is good\n/file: string is great", grepOutput);
  }

  @Test
  public void testOneLineWithPath() throws Exception {
    system.addFileSystem("folder");
    main.setInput("echo one string");
    output = main.execute(system);

    secondary.setInput("echo one string > folder/file");
    FC.execute(system, secondary.getArguments(), output);

    grep = new Grep("");
    grep.setInput("grep string folder/file");
    grepOutput = grep.execute(system);

    assertEquals("/folder/file: one string", grepOutput);
  }

  @Test
  public void testRecursive() throws Exception {
    main.setInput("echo one string");
    output = main.execute(system);
    secondary.setInput("echo one string > file");
    FC.execute(system, secondary.getArguments(), output);

    main.setInput("echo two string");
    output = main.execute(system);
    secondary.setInput("echo two string > file2");
    FC.execute(system, secondary.getArguments(), output);

    main.setInput("echo three string");
    output = main.execute(system);
    secondary.setInput("echo three string >> file2");
    FC.execute(system, secondary.getArguments(), output);

    grep = new Grep("");
    grep.setInput("grep -R string /");
    grepOutput = grep.execute(system);
    expected =
        "/file: one string\n" + "/file2: two string\n" + "/file2: three string";
    assertEquals(expected, grepOutput);
  }

  @Test
  public void testRecursiveForParentDirectory() throws Exception {
    main.setInput("echo one string");
    output = main.execute(system);
    secondary.setInput("echo one string > file");
    FC.execute(system, secondary.getArguments(), output);

    main.setInput("echo two string");
    output = main.execute(system);
    secondary.setInput("echo two string > file2");
    FC.execute(system, secondary.getArguments(), output);

    main.setInput("echo three string");
    output = main.execute(system);
    secondary.setInput("echo three string >> file2");
    FC.execute(system, secondary.getArguments(), output);

    system.addFileSystem("folder");
    cd = new CD("cd folder");
    system = cd.changPath(system);

    grep = new Grep("");
    grep.setInput("grep -R string ..");
    grepOutput = grep.execute(system);
    expected =
        "/file: one string\n" + "/file2: two string\n" + "/file2: three string";
    assertEquals(expected, grepOutput);
  }

  @Test
  public void testRecursiveWithOneDot() throws Exception {
    main.setInput("echo one string");
    output = main.execute(system);
    secondary.setInput("echo one string > file");
    FC.execute(system, secondary.getArguments(), output);

    main.setInput("echo two string");
    output = main.execute(system);
    secondary.setInput("echo two string > file2");
    FC.execute(system, secondary.getArguments(), output);

    main.setInput("echo three string");
    output = main.execute(system);
    secondary.setInput("echo three string >> file2");
    FC.execute(system, secondary.getArguments(), output);

    grep = new Grep("");
    grep.setInput("grep -R string .");
    grepOutput = grep.execute(system);
    expected =
        "/file: one string\n" + "/file2: two string\n" + "/file2: three string";
    assertEquals(expected, grepOutput);
  }

}
