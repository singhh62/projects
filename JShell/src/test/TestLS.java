// **********************************************************
// Assignment2:
// Student1: Hardeep Singh
// UTOR user_name: singhh62
// UT Student #: 999741793
// Author: Hardeep Singh
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
// *********************************************************

package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Commands.*;
import driver.*;
import Helper.*;

/**
 * 
 * @author Spasimir
 *
 */
public class TestLS {
  PathHandler path;
  Commands main;
  String[] str;
  FileSystem FS;

  @Before
  public void setUp() {
    path = new PathHandler();
    main = new LS("");
    FS = new FileSystem("aaa");

  }

  @Test
  /*
   * test if ls lists the files and folders in the current directory
   */
  public void testExecuteWithoutPath() throws Exception {
    str = new String[0];
    FS.addFileSystem("Folder");
    FS.addFile("File", false, "content");
    main.setArgument(str);
    String executer = main.execute(FS);
    assertEquals("Folder\tFile", executer);
  }

  @Test
  /*
   * test if ls lists the files and folder in the current directory when there
   * are many files and folders
   */
  public void testExecuteWithoutPathWithMoreFiles() throws Exception {
    str = new String[0];
    FS.addFileSystem("Folder");
    FS.addFileSystem("Folder2");
    FS.addFileSystem("Folder3");
    FS.addFile("File", false, "content");
    FS.addFile("File2", false, "content2");
    FS.addFile("File3", false, "content3");
    main.setArgument(str);
    String executer = main.execute(FS);
    assertEquals("Folder3\tFolder2\tFolder\tFile2\tFile\tFile3", executer);
  }

  @Test
  /*
   * test if ls lists the files and folders with a specified path
   */
  public void testExecuteWithPath() throws Exception{
    FS.addFileSystem("Folder");
    FS.getChildrenFS().get("Folder").addFileSystem("Folder2");
    FS.getChildrenFS().get("Folder").addFile("File", false, "aa");
    str = new String[1];
    str[0] = "Folder";
    main.setArgument(str);
    String executer = main.execute(FS);
    assertEquals("Directory \"Folder\" has"+"\nFolder2\tFile", executer);
  }

  @Test
  /*
   * test if ls lists the files and folders with a specified path that is long
   */
  public void testExecuteWithLongerPath() throws Exception {
    FS.addFileSystem("F");
    FS.getChildrenFS().get("F").addFileSystem("F2");
    FS.getChildrenFS().get("F").getChildrenFS().get("F" + "2")
        .addFileSystem("F3");
    FS.getChildrenFS().get("F").getChildrenFS().get("F" + "2").addFile("File",
        false, "asdd");
    str = new String[1];
    str[0] = "F/F2";
    main.setArgument(str);
    String executer = main.execute(FS);
    assertEquals("Directory \"" + "F2" + "\"" + " has\nF3\tFile", executer);
  }


}
