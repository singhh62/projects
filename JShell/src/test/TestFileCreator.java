// **********************************************************
// Assignment2:
// Student1: Hardeep Singh
// UTOR user_name: singhh62
// UT Student #: 999741793
// Author: Hardeep Singh
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//
package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Commands.CD;
import Commands.Cat;
import Commands.Echo;
import Commands.LS;
import Commands.PWD;
import Helper.Commands;
import Helper.FileCreator;
import driver.FileSystem;
/**
 * 
 * @author Spasimir
 *
 */
public class TestFileCreator {
  FileSystem system;
  FileCreator creator;
  Commands main;
  Commands test;
  String executer;
  String contents;
  CD cd;

  @Before
  public void setUp() throws Exception {
    system = new FileSystem("");
    creator = new FileCreator("");
    test = new Commands();
    
  }

  @Test
  /*
   * test if FileCreator correctly writes a string to a file using the output
   * from LS
   */
  public void testWrite() throws Exception {
    system.addFileSystem("a");
    system.addFileSystem("b");
    main = new LS("");
    main.setInput("ls");
    test.setInput("ls > file");
    executer = main.execute(system);
    creator.execute(system, test.getArguments(), executer);
    assertEquals(true, system.isFileExist("file"));
    contents = system.getChildrenFile().get("file").getContent();
    assertEquals("b\ta", contents);
  }
  
  @Test
  /*
   * test if FileCreator correctly writes a string to a file with a path
   * using the output from LS
   */
  public void testWriteWithPath() throws Exception {
    system.addFileSystem("a");
    system.addFileSystem("b");
    main = new LS("");
    main.setInput("ls");
    test.setInput("ls > a/file");
    executer = main.execute(system);
    creator.execute(system, test.getArguments(), executer);
    cd = new CD("cd a");
    FileSystem aaa = cd.changPath(system);
    assertEquals(true, aaa.isFileExist("file"));
    contents = aaa.getChildrenFile().get("file").getContent();
    assertEquals("b\ta", contents);
  }
  
  @Test
  /*
   * test if FileCreator correctly overwrites the content of a file
   */
  public void testOverwrite() throws Exception {
    system.addFileSystem("a");
    system.addFileSystem("b");
    main = new LS("");
    main.setInput("ls");
    test.setInput("ls > file");
    executer = main.execute(system);
    creator.execute(system, test.getArguments(), executer);
    
    executer = main.execute(system);
    creator.execute(system, test.getArguments(), executer);
    
    assertEquals(true, system.isFileExist("file"));
    contents = system.getChildrenFile().get("file").getContent();
    assertEquals("b\ta\tfile", contents);
  }
  
  @Test
  /*
   * test if FileCreator correctly overwrites the content of a file with a path
   */
  public void testOverwriteWithPath() throws Exception {
    system.addFileSystem("a");
    system.addFileSystem("b");
    main = new LS("");
    main.setInput("ls");
    test.setInput("ls > a/file");
    executer = main.execute(system);
    creator.execute(system, test.getArguments(), executer);
    cd = new CD("cd a");
    FileSystem aaa = cd.changPath(system);
    
    assertEquals(true, aaa.isFileExist("file"));
    contents = aaa.getChildrenFile().get("file").getContent();
    assertEquals("b\ta", contents);
    
    system.addFileSystem("c");
    executer = main.execute(system);
    creator.execute(system, test.getArguments(), executer);
    
    assertEquals(true, aaa.isFileExist("file"));
    contents = aaa.getChildrenFile().get("file").getContent();
    assertEquals("b\ta\tc", contents);
  }
  
  @Test
  /*
   * test if FileCreator correctly overloads the contents of a file
   */
  public void testOverload() throws Exception {
    system.addFileSystem("a");
    system.addFileSystem("b");
    main = new LS("");
    main.setInput("ls");
    test.setInput("ls > file");
    executer = main.execute(system);
    creator.execute(system, test.getArguments(), executer);
    assertEquals(true, system.isFileExist("file"));
    contents = system.getChildrenFile().get("file").getContent();
    assertEquals("b\ta", contents);
    
    test.setInput("ls >> file");
    executer = main.execute(system);
    creator.execute(system, test.getArguments(), executer);
    contents = system.getChildrenFile().get("file").getContent();
    assertEquals("b\ta\nb\ta\tfile", contents);
  }
  
  @Test
  /*
   * test if FileCreator correctly overloads the contents of a file with a path
   */
  public void testOverloadWithPath() throws Exception {
    system.addFileSystem("a");
    system.addFileSystem("b");
    main = new LS("");
    main.setInput("ls");
    
    test.setInput("ls > a/file");
    executer = main.execute(system);
    creator.execute(system, test.getArguments(), executer);
    cd = new CD("cd a");
    FileSystem aaa = cd.changPath(system);
    
    assertEquals(true, aaa.isFileExist("file"));
    contents = aaa.getChildrenFile().get("file").getContent();
    assertEquals("b\ta", contents);
    
    test.setInput("ls >> a/file");
    executer = main.execute(system);
    creator.execute(system, test.getArguments(), executer);
    contents = aaa.getChildrenFile().get("file").getContent();
    assertEquals("b\ta\nb\ta", contents);
  }
  
  @Test
  /*
   * test if FileCreator correctly writes the output of PWD inside a file
   */
  public void testWriteWithPWD() throws Exception {
    system.addFileSystem("a");
    system.addFileSystem("b");
    cd = new CD("cd a");
    system = cd.changPath(system);
    main = new PWD("");
    main.setInput("pwd");
    
    test.setInput("pwd > file");
    executer = main.execute(system);
    creator.execute(system, test.getArguments(), executer);
    assertEquals(true, system.isFileExist("file"));
    contents = system.getChildrenFile().get("file").getContent();
    assertEquals("/a", contents);
  }
  
  @Test
  /*
   * test if FileCreator correctly writes the output of Cat inside a file
   */
  public void testWriteWithCat() throws Exception {
    system.addFileSystem("a");
    system.addFileSystem("b");
    system.addFile("file", false, "content");
    main = new Cat("");
    main.setInput("cat file");
    
    test.setInput("cat > file2");
    executer = main.execute(system);
    creator.execute(system, test.getArguments(), executer);
    assertEquals(true, system.isFileExist("file2"));
    contents = system.getChildrenFile().get("file2").getContent();
    String expected = "File \"file\" contains: \n\ncontent\n";
    assertEquals(expected, contents);
  }
  
  @Test
  /*
   * test if FileCreator correctly writes the output of Echo inside a file
   */
  public void testWriteWithEcho() throws Exception {
    system.addFileSystem("a");
    system.addFileSystem("b");
    system.addFile("file", false, "content");
    main = new Echo("");
    main.setInput("echo string");
    
    test.setInput("echo string > file");
    executer = main.execute(system);
    creator.execute(system, test.getArguments(), executer);
    assertEquals(true, system.isFileExist("file"));
    contents = system.getChildrenFile().get("file").getContent();
    assertEquals("string", contents);
  }
  
  
  

}
