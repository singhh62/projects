// **********************************************************
// Assignment2:
// Student1: Hardeep Singh
// UTOR user_name: singhh62
// UT Student #: 999741793
// Author: Hardeep Singh
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
// *********************************************************

package test;

import static org.junit.Assert.*;

import java.util.Stack;

import org.junit.Before;
import org.junit.Test;

import Commands.CD;
import Commands.PWD;
import Helper.Commands;
import Helper.DirectoryStack;
import Helper.PathHandler;
import driver.FileSystem;
/**
 * 
 * @author Spasimir
 *
 */
public class TestDirectoryStack {
  PathHandler path;
  DirectoryStack dir;
  String [] str;
  FileSystem FS;
  String [] args;
  Commands pwd;
  String executer;
  CD cd;
  Stack<String> stack;

  @Before
  public void setUp(){
    path = new PathHandler();
    dir = new DirectoryStack();
    FS = new FileSystem("root");
    args = new String[1];
    pwd = new PWD("");
    pwd.setArgument(new String[0]);
    stack = new Stack<String>();
  }

  @Test
  /*
   * test if pushd ads the file system to a stack
   */
  public void testPushD() throws Exception {
    FS.addFileSystem("Folder");
    cd = new CD("cd Folder");
    dir.add(FS);
    stack.push("/");
    FS = cd.changPath(FS);
    executer = pwd.execute(FS);
    assertEquals("/Folder", executer);
    assertEquals(stack, dir.getStack());
  }
  @Test
  /*
   * test if popd pops a file system from the stack
   */
  public void testPopD() throws Exception{
    FS.addFileSystem("Folder");
    FS.getChildrenFS().get("Folder").addFileSystem("Folder2");
    cd = new CD("cd Folder");
    dir.add(FS);
    stack.push("/");
    FS = cd.changPath(FS);
    cd = new CD("cd Folder2");
    dir.add(FS);
    stack.push("Folder");
    FS = cd.changPath(FS);
    cd = new CD("cd ..");
    dir.remove();
    stack.pop();
    FS = cd.changPath(FS);
    executer = pwd.execute(FS);
    assertEquals("/Folder", executer);
    assertEquals(stack, dir.getStack());
    
  }

}
