// **********************************************************
// Assignment2:
// Student1: Hardeep Singh
// UTOR user_name: singhh62
// UT Student #: 999741793
// Author: Hardeep Singh
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
// *********************************************************
/*
 * author: Anees Bajwa
 */
package test;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import Helper.Commands;


public class TestCommand {
  Commands cmd;

  @Before
  public void setUp() throws Exception {
    cmd = new Commands();
  }

  @Test
  public void testGetCommand() {
    cmd.setCommand("mkdir anees");
    assertEquals("mkdir anees", cmd.getCommand());
  }
  
  @Test
  public void testGetArgument(){
    cmd.setCommand("cmd arg1 arg2");
    String[] arg = cmd.getCommand().trim().split(" +");
    cmd.setArgument(arg);
    assertArrayEquals(arg, cmd.getArguments());
  }
  
  @Test
  public void testSetInput(){
    String input = "cmd arg1 arg2";
    String[] inputSeperator = input.trim().split(" +");
    cmd.setCommand(inputSeperator[0]);
    cmd.setArgument(Arrays.copyOfRange(inputSeperator, 1, 
        inputSeperator.length));
    String[] expectedValue = {"arg1", "arg2"};
    assertEquals("cmd", cmd.getCommand());
    assertArrayEquals(expectedValue, cmd.getArguments());
  }
  
  @Test
  public void testSetArgument(){
    String[] arguments = {"arg1", "arg2"};
    cmd.setArgument(arguments);
    assertArrayEquals(arguments, cmd.getArguments());
  }

  @Test
  public void testSetCommand(){
    String command = "cmd";
    cmd.setCommand(command);
    assertEquals(command, cmd.getCommand());
  }
}
