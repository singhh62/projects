// **********************************************************
// Assignment2:
// Student1: Hardeep Singh
// UTOR user_name: singhh62
// UT Student #: 999741793
// Author: Hardeep Singh
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
// *********************************************************

package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import Commands.*;
import driver.*;
import Helper.*;
/**
 * 
 * @author Spasimir
 *
 */
public class TestCD {
  PathHandler path;
  CD main;
  String [] str;
  FileSystem FS;
  String [] args;
  Commands pwd;
  String executer;
  
  @Before
  public void setUp(){
    path = new PathHandler();
    main = new CD("asd");
    FS = new FileSystem("aaa");
    args = new String[1];
    pwd = new PWD("");
    pwd.setArgument(new String[0]);
  }
  @Test
  /*
   * test if the method setPath works correctly
   */
  public void testSetPath() {
    main.setArgument(args);
    main.setPath("thePath");
    assertEquals(main.getArgumentAtPosition(0), "thePath");
  }
  
  @Test
  /*
   * test if cd changes the directory to a folder
   */
  public void testChangPath() throws Exception{
    FS.addFileSystem("Folder");
    main = new CD("cd Folder");
    FS = main.changPath(FS);
    executer = pwd.execute(FS);
    assertEquals("/Folder", executer);
  }
  
  @Test
  /*
   * test if cd changes the directory to a folder inside a folder
   */
  public void testChangPathWithLongerPath() throws Exception{
    FS.addFileSystem("Folder");
    FS.getChildrenFS().get("Folder").addFileSystem("Folder2");
    main = new CD("cd Folder/Folder2");
    FS = main.changPath(FS);
    executer = pwd.execute(FS);
    assertEquals("/Folder/Folder2", executer);
  }
  
  @Test
  /*
   * test if cd changes the directory to the parent directory 
   */
  public void testChangPathDoubleDot() throws Exception{
    FS.addFileSystem("Folder");
    main = new CD("cd Folder");
    FS = main.changPath(FS);
    main = new CD("cd ..");
    FS = main.changPath(FS);
    executer = pwd.execute(FS);
    assertEquals("/", executer);
  }
  
  @Test
  public void testChangPathDoubleDotMoreLevels() throws Exception{
    FS.addFileSystem("Folder");
    FS.getChildrenFS().get("Folder").addFileSystem("Folder2");
    main = new CD("cd Folder/Folder2");
    FS = main.changPath(FS);
    main = new CD("cd ..");
    FS = main.changPath(FS);
    executer = pwd.execute(FS);
    assertEquals("/Folder", executer);
    
  }
  
  @Test
  /*
   * test if cd keeps the same directory when . is typed
   */
  public void testChangPathSingleDot() throws Exception{
    FS.addFileSystem("Folder");
    main = new CD("cd Folder");
    FS = main.changPath(FS);
    main = new CD("cd .");
    FS = main.changPath(FS);
    executer = pwd.execute(FS);
    assertEquals("/Folder", executer);
  }
  
}
