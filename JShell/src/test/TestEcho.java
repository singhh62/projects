// **********************************************************
// Assignment2:
// Student1: Hardeep Singh
// UTOR user_name: singhh62
// UT Student #: 999741793
// Author: Hardeep Singh
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
// *********************************************************

package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Commands.Cat;
import Commands.Echo;
import Helper.Commands;
import Helper.PathHandler;
import driver.FileSystem;
/**
 * 
 * @author Spasimir
 *
 */
public class TestEcho {
  Commands main;
  FileSystem FS;

  @Before
  public void setUp(){
    main = new Echo("");
    FS = new FileSystem("");
  }

  @Test
  /*
   * test if echo works with a given string
   */
  public void testEcho() throws Exception{
    main.setInput("echo here is a string");
    String result = main.execute(FS);
    assertEquals("here is a string", result);
  }

}
