// **********************************************************
// Assignment2:
// Student1: Hardeep Singh
// UTOR user_name: singhh62
// UT Student #: 999741793
// Author: Hardeep Singh
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
// *********************************************************

package test;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

import Commands.*;
import driver.*;
import Helper.*;
/**
 * 
 * @author Spasimir
 *
 */
public class TestCat {
  PathHandler path;
  Commands main;
  String [] str;
  FileSystem FS;
  
  @Before
  public void setUp(){
    path = new PathHandler();
    main = new Cat("");
    str = new String[10];
    FS = new FileSystem("aaa");
  }

  @Test
  /*
   * test if cat returns the content when it is only one word
   */
  public void testWithOneWordContent() throws Exception {
    FS.addFile("hello", false, "test");
    str[0] = "hello";
    main.setArgument(str);
    String executer = main.execute(FS);
    executer = executer.trim();
    assertEquals("File \"hello\" contains: \n\ntest", executer);
  }
  @Test
  /*
   * test if cat returns the content when the file is empty
   */
  public void testCatEmptyFile() throws Exception{
    FS.addFile("hello", false, "");
    str[0] = "hello";
    main.setArgument(str);
    String executer = main.execute(FS);
    executer = executer.trim();
    assertEquals("File \"hello\" contains:", executer);
  }
  @Test
  /*
   * test if cat returns the content when the file has multiple words
   */
  public void testWithMultipleWordsContent() throws Exception{
    FS.addFile("hello", false, "this is a test");
    str[0] = "hello";
    main.setArgument(str);
    String executer = main.execute(FS);
    executer = executer.trim();
    assertEquals("File \"hello\" contains: \n\nthis is a test", executer);
  }
  @Test
  /*
   * test if cat returns the content when the file has different cymbols
   */
  public void testWithDifferentSymbols() throws Exception{
    FS.addFile("hello", false, "this..is///a/:'test");
    str[0] = "hello";
    main.setArgument(str);
    String executer = main.execute(FS);
    executer = executer.trim();
    assertEquals("File \"hello\" contains: \n\nthis..is///a/:'test", executer);
  }
  @Test
  /*
   * test if cat returns the content when the file has miltiple new lines and
   * tabs
   */
  public void testWithNewLinesAndTabs() throws Exception{
    FS.addFile("hello", false, "this\nis\n\t/a/:'test");
    str[0] = "hello";
    main.setArgument(str);
    String executer = main.execute(FS);
    executer = executer.trim();
    assertEquals("File \"hello\" contains: \n\nthis\nis\n\t/a/:'test", executer);
  }
  
 }
