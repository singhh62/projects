// **********************************************************
// Assignment2:
// Student1: Hardeep Singh
// UTOR user_name: singhh62
// UT Student #: 999741793
// Author: Hardeep Singh
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//

package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Commands.History;
import Helper.Log;

/**
 * @author Raj
 *
 */
public class TestLog {
  private History history;
  private Log log;

  @Before
  public void setUp() throws Exception {
    log = new Log();
  }

  @Test
  // Test history command when only command is entered
  public void test_One_log() throws Exception {
    log.setNewInput("mkdir a");
    log.setNewInput("history");
    history = new History("history");
    assertEquals(history.execute(log), "1 mkdir a\n2 history");
  }

  @Test
  // Test history command with no arguments but more than 1 command
  public void test_Multiple_logs() throws Exception {
    log.setNewInput("mkdir a");
    log.setNewInput("cd a");
    log.setNewInput("mkdir a b c");
    log.setNewInput("history");
    history = new History("history");
    assertEquals(history.execute(log),
        "1 mkdir a\n2 cd a\n3 mkdir a b c\n4 history");
  }

  @Test
  // Test the history command with argument 2 given
  public void test_history_num() throws Exception {
    log.setNewInput("mkdir a");
    log.setNewInput("cd a");
    log.setNewInput("mkdir a b c");
    log.setNewInput("history");
    history = new History("history 2");
    assertEquals(history.execute(log), "3 mkdir a b c\n4 history");
  }

  @Test
  // Test when there is no commands entered
  public void test_no_command() throws Exception {
    history = new History("history");
    assertEquals(history.execute(log), "");
  }

  @Test
  // Test when incorrect commands are entered
  public void test_inccorrect_arg() throws Exception {
    log.setNewInput("mkdir adad");
    log.setNewInput("brack");
    log.setNewInput("history");
    history = new History("history");
    assertEquals(history.execute(log), "1 mkdir adad\n2 brack\n3 history");
  }
}
