// **********************************************************
// Assignment2:
// Student1: Hardeep Singh
// UTOR user_name: singhh62
// UT Student #: 999741793
// Author: Hardeep Singh
//
// Student2: Raj Shah
// UTOR user_name: shahraj
// UT Student #: 1001192265
// Author: Raj Shah
//
// Student3: Spasimir Vasilev
// UTOR user_name: vasile14
// UT Student #: 1001719624
// Author: Spasimir Vasilev
//
// Student4: Anees Bajwa
// UTOR user_name: bajwaane
// UT Student #: 1001849556
// Author: Anees Bajwa
//
//
// Honor Code: I pledge that this program represents my own
// program code and that I have coded on my own. I received
// help from no one in designing and debugging my program.
// I have also read the plagiarism section in the course info
// sheet of CSC 207 and understand the consequences.
//
package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Commands.Echo;
import Commands.PWD;
import Helper.Commands;
import Helper.Redirector;
/**
 * 
 * @author Spasimir
 *
 */
public class TestRedirector {
  Redirector red;
  Commands cmd;

  @Before
  public void setUp() throws Exception {
    red = new Redirector();
  }

  @Test
  /*
   * test if getNewCommand returns the proper output with a simple command
   */
  public void testSimpleCommand() {
    cmd = new PWD("");
    String something = "pwd > file";
    cmd.setInput(something);
    String[] arguments = cmd.getArguments();
    String newSomething = red.getNewCommand(something, arguments);
    assertEquals("pwd", newSomething);
  }
  
  @Test
  /*
   * test if getNewCommand returns the proper output with a weird command
   */
  public void TestComplicatedCommand(){
    cmd = new PWD("");
    String something = "pwd >> file > asd";
    cmd.setInput(something);
    String[] arguments = cmd.getArguments();
    String newSomething = red.getNewCommand(something, arguments);
    assertEquals("pwd", newSomething);
  }
  
  @Test
  /*
   * test if getNewCommand returns the proper output when there is no 
   * redirection
   */
  public void testCommandNoRedirecton() {
    cmd = new PWD("");
    String something = "pwd";
    cmd.setInput(something);
    String[] arguments = cmd.getArguments();
    String newSomething = red.getNewCommand(something, arguments);
    assertEquals("pwd", newSomething);
  }
  
  @Test
  /*
   * test if getNewCommand returns the proper output when the argument list is
   * of length 1
   * 
   */
  public void testCommandWithSmallArgList() {
    cmd = new PWD("");
    String something = "pwd line";
    cmd.setInput(something);
    String[] arguments = cmd.getArguments();
    String newSomething = red.getNewCommand(something, arguments);
    assertEquals("pwd line", newSomething);
  }
  
  @Test
  /*
   * test if isRedirect returns the proper value
   */
  public void TestIsRedirect(){
    cmd = new PWD("");
    String something = "pwd >> file > asd";
    cmd.setInput(something);
    String[] arguments = cmd.getArguments();
    boolean redirect = red.isRedirect(something, arguments);
    assertEquals(true, redirect);
  }
  
  @Test
  /*
   * test if isRedirect returns the proper value when there is no redirection
   */
  public void TestIsNotRedirect(){
    cmd = new PWD("");
    String something = "pwd";
    cmd.setInput(something);
    String[] arguments = cmd.getArguments();
    boolean redirect = red.isRedirect(something, arguments);
    assertEquals(false, redirect);
  }
  
  @Test
  /*
   * test if isRedirect returns the proper value when there is no file 
   * specified
   */
  public void TestIsNotRedirectWithNoFile(){
    cmd = new Echo("");
    String something = "echo string >";
    cmd.setInput(something);
    String[] arguments = cmd.getArguments();
    boolean redirect = red.isRedirect(something, arguments);
    assertEquals(false, redirect);
  }
  
  @Test
  /*
   * test if isRedirect returns the proper value when the symbol used is 
   * invalid
   */
  public void TestIsRedirectInvalidSymbol(){
    cmd = new PWD("");
    String something = "pwd >>>> file";
    cmd.setInput(something);
    String[] arguments = cmd.getArguments();
    boolean redirect = red.isRedirect(something, arguments);
    assertEquals(false, redirect);
  }

  

}
