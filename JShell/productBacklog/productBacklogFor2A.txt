User-centric:		
						
1)As a user, I would want to use mkdir command so that I can make a new directory. (3 days) 
2)As a user, I would want to change directory using the cd command. (2 days)
3)As a user, I would want to list the path's content using ls command. (1 day)
4)As a user, I would want to print the current working directory using pwd 
   Command. (1 day)
5)As a user I would want to save the current working directory to a stack, and 
   change the directory to the argument passed through. (2 days)
6)As a user, I would want to go back to the previous directory. (1 day)
7)As a user, I would want want to print the recently used commands. (2 days)
8)As a user, I would want to display the file content using cat. (2 days)
9)As a user, I would want to input a string into a file and overwrite (keep 
  everything) using the command echo. (1 day)
10) As a user, I would like to print documentation for a command by man command (1 day)
11) As a user, I am expecting to close the program by the exit command. (1 day)

Developer-centric:

1)As a developer, I would want to prompt for user input.(1 day) 
2)As a developer, I would want to implement command class. (2 days)
3)As a developer, I would want to implement each of my CRC cards. (3 days)
4)As a developer, I would want to test each command. (1 day)
5)As a developer, I would want to check every user input whether it is valid (1 day)
6)As a developer, I would want to handle paths from user input. (2 days)

Functional Requirements:

1)The code should work with multiple white spaces.
2)The arguments ".."(Parent), "."(Current) and "/"(root) should work in every 
  command.
3)The code should not break at any time except under exit command. 
4)The code is implemented under the google java design.
5)The code is prompting properly regard handout's requirement. 