// Note: Yet another way to declare a class, using .prototype.
function Stage(width, height, stageElementID) {
    this.actors = []; // all actors on this stage (monsters, player, boxes, ...)
    this.player = null; // a special actor, the player
    
    // the logical width and height of the stage
    this.width = width;
    this.height = height;
    this.time = 0;
    this.monstercount = 0;
    this.pause = false;
    
    // the element containing the visual representation of the stage
    this.stageElementID = stageElementID;
    
    // take a look at the value of these to understand why we capture them this way
    // an alternative would be to use 'new Image()'
    this.blankImageSrc = document.getElementById('blankImage').src;
    this.monsterImageSrc = document.getElementById('monsterImage').src;
    this.playerImageSrc = document.getElementById('playerImage').src;
    this.boxImageSrc = document.getElementById('boxImage').src;
    this.wallImageSrc = document.getElementById('wallImage').src;
}

// initialize an instance of the game
Stage.prototype.initialize = function() {
    // Create a table of blank images, give each image an ID so we can reference it later
    var s = "<table cellspacing='0'>";
    // YOUR CODE GOES HERE
    for (var h = 0; h < this.height; h++) {
        s += "<tr>";
        for (var w = 0; w < this.width; w++) {
            s += '<td style="border:1px solid black;"><image src = "' + this.blankImageSrc + '" id = "' + this.getStageId(w, h) + '" /></td>';
        }
        s += "</tr>";
    }
    s += "</table>";
    
    // Put it in the stageElementID (innerHTML)
    document.getElementById(this.stageElementID).innerHTML = s;
    
    // Add the player to the center of the stage
    this.player = new Player(this, Math.round((this.width / 2)), Math.round((this.height / 2)));
    this.addActor(this.player);
    
    // Add walls around the outside of the stage, so actors can't leave the stage
    for (var y = 0; y < this.height; y++) {
        for (var x = 0; x < this.height; x++) {
            if (x == 0 || x == this.width - 1 || y == 0 || y == this.height - 1) {
                this.addActor(new Wall(this, x, y));
            }
        }
    }
    
    // Add some Boxes to the stage
    var maxd = Math.round((this.height * this.width) * 0.2);
    var i = 0;
    while (i < maxd) {
        rx = ((Math.random() * ((x - 2))) << 0) + 1;
        ry = ((Math.random() * ((y - 2))) << 0) + 1;
        if (document.getElementById(this.getStageId(rx, ry)).src == this.blankImageSrc) {
            this.addActor(new Box(this, rx, ry));
            i++;
        }
    }
    // Add in some Monsters
    var k = 0; //monsters
    
    while (k < 12) { //12 ENEMIES
        rx = ((Math.random() * ((x - 2))) << 0) + 1;
        ry = ((Math.random() * ((y - 2))) << 0) + 1;
        if (document.getElementById(this.getStageId(rx, ry)).src == this.blankImageSrc) {
            this.addActor(new Monster(this, rx, ry));
            k++;
        }
    }
    stage.controls(stage.player, true);
    
}
// Return the ID of a particular image, useful so we don't have to continually reconstruct IDs
Stage.prototype.getStageId = function(x, y) {
    return "stage_" + x + "-" + y;
}

Stage.prototype.addActor = function(actor) {
    this.actors.push(actor);
    if (actor instanceof Player) {
        this.player = actor;
    }
    if (actor instanceof Monster) {
        this.monstercount++;
    }
}

Stage.prototype.removeActor = function(actor) {
    this.setImage(actor.x, actor.y, this.blankImageSrc);
    this.actors.splice(this.actors.indexOf(actor), 1);
    
    if (actor instanceof Player) {
        this.player = null;
    }
    if (actor instanceof Monster) {
        this.monstercount--;
    }
    // Lookup javascript array manipulation (indexOf and splice).
}

// Set the src of the image at stage location (x,y) to src
Stage.prototype.setImage = function(x, y, src) {
    document.getElementById(this.getStageId(x, y)).src = src;
}

// Take one step in the animation of the game.
Stage.prototype.step = function() {
    if (this.player == null) { //Means that the monster has hit it
        try {
            stage.controls(stage.player, false);
        } catch (err) {}
        var check = "highscoree";
        var user = $("#user").val();
        var highscore = this.time;
        var request = $.ajax({
            url: "api/api.php",
            method: "PUT",
            data: JSON.stringify({
                user: user,
                check: check,
                highscore: highscore
            }),
        });
        
        request.done(function(msg) {
            alert("You Lost!, Your Score was: " + highscore);
            document.getElementById("tr").style = "display:none;";
            document.getElementById("my").style = "display:block;";
            clearInterval(interval);
            interval = null;
            document.getElementById("welcome").style = "display:block;";
            document.getElementById("highscorediv").style = "display:block";
            getScorenew();
        });
        
        request.fail(function(jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
        
        this.player = new Player(this, Math.round((this.width / 2)), Math.round((this.height / 2)));
        this.addActor(this.player);
        return;
        
    } else if (this.monstercount == 0) {
        try {
            stage.controls(stage.player, false);
        } catch (err) {}
        var check = "highscoree";
        var user = $("#user").val();
        var highscore = this.time + 20;
        var request = $.ajax({
            url: "api/api.php",
            method: "PUT",
            data: JSON.stringify({
                user: user,
                check: check,
                highscore: highscore
            }),
        });
        
        request.done(function(msg) {
            alert("YOU WIN!!, Your Score was: " + highscore);
            document.getElementById("tr").style = "display:none;";
            document.getElementById("my").style = "display:block;";
            clearInterval(interval);
            interval = null;
            document.getElementById("welcome").style = "display:block;";
            document.getElementById("highscorediv").style = "display:block";
            getScorenew();
        });
        
        request.fail(function(jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
        
        this.player = new Player(this, Math.round((this.width / 2)), Math.round((this.height / 2)));
        this.addActor(this.player);
        return;
        
    } else if (this.pause == false) {
        for (var i = 0; i < this.actors.length; i++) {
            this.actors[i].step();
        }
        this.time++;
        document.getElementById("timeElapsed").value = this.time;
    }
}

Stage.prototype.controls = function(player, run) {
    if (run == true) {
        document.onkeypress = function(e) {
            if (e.charCode == 113) {
                player.move(player, -1, -1);
            } else if (e.charCode == 119) {
                player.move(player, 0, -1);
            } else if (e.charCode == 101) {
                player.move(player, 1, -1);
            } else if (e.charCode == 97) {
                player.move(player, -1, 0);
            } else if (e.charCode == 100) {
                player.move(player, 1, 0);
            } else if (e.charCode == 122) {
                player.move(player, -1, 1);
            } else if (e.charCode == 120) {
                player.move(player, 0, 1);
            } else if (e.charCode == 99) {
                player.move(player, 1, 1);
            }
        };
    } else {
        document.onkeypress = null;
        return;
    }
}

// return the first actor at coordinates (x,y) return null if there is no such actor
// there should be only one actor at (x,y)!
Stage.prototype.getActor = function(x, y) {
    var f = null;
    for (var i = 0; i < this.actors.length; i++) {
        if (this.actors[i].x == x && this.actors[i].y == y) {
            f = this.actors[i];
            break;
        }
    }
    return f;
}
// End Stage Class

// Start Player Class
function Player(stage, x, y) {
    
    this.stage = stage;
    this.stage.setImage(x, y, this.stage.playerImageSrc);
    this.x = x;
    this.y = y;
}
Player.prototype.step = function() {
    
    return;
}

Player.prototype.move = function(player, changex, changey) {
    newPlayer = this.stage.getActor(this.x + changex, this.y + changey);
    if (player instanceof Monster || newPlayer instanceof Monster) { //If player comes interacts with Monsters
        this.stage.removeActor(player);
        return false;
    } else {
        if (newPlayer == null || newPlayer.move(this, changex, changey)) {
            this.stage.removeActor(this);
            this.x += changex;
            this.y += changey;
            this.stage.addActor(this);
            this.stage.setImage(this.x, this.y, this.stage.playerImageSrc);
            return true;
        } else {
            return false;
        }
    }
} //END Player class


// Start Wall Class
function Wall(stage, x, y) {
    this.x = x;
    this.y = y;
    this.stage = stage;
    this.stage.setImage(x, y, this.stage.wallImageSrc);
}

Wall.prototype.step = function() {
    return;
} // End Wall class


// Start Box Class
function Box(stage, x, y) {
    this.x = x;
    this.y = y;
    this.stage = stage;
    this.stage.setImage(x, y, this.stage.boxImageSrc);
}
Box.prototype.step = function() {
    return;
}
Box.prototype.move = function(box, changex, changey) {
    newBox = this.stage.getActor(changex + this.x, changey + this.y);
    if (box instanceof Player || box instanceof Box) {
        if (newBox == null || newBox.move(this, changex, changey)) {
            this.stage.removeActor(this);
            this.x += changex;
            this.y += changey;
            this.stage.addActor(this);
            this.stage.setImage(this.x, this.y, this.stage.boxImageSrc);
            return true;
        }
    }
    return false;
    
}
function Monster2(stage, x, y){
    this.x = x;
    this.y = y
    this.xmoves = [-1, 0, 1, -1, 1, -1, 0, 1];
    this.ymoves = [-1, -1, -1, 0, 0, 1, 1, 1];
    this.stage = stage;
    this.stage.setImage(x, y, this.stage.monsterImageSrc);
}
// Start Monster class (fixed one)

function Monster(stage, x, y) {
    this.x = x;
    this.y = y;
    
    this.xmoves = [-1, 0, 1, -1, 1, -1, 0, 1];
    this.ymoves = [-1, -1, -1, 0, 0, 1, 1, 1];
    this.stage = stage;
    this.stage.setImage(x, y, this.stage.monsterImageSrc);
}


Monster.prototype.step = function() {
    var total = 0;
    for(var i = 0; i < 8; i++) {
        direction = this.stage.getActor(this.x + this.xmoves[i], this.y + this.ymoves[i]);
        if(direction instanceof Box || direction instanceof Wall) {
            total++;
        }
    }
    if (total == 8) {
        this.stage.removeActor(this);
    }
    else {
        var check = false;
        while(check == false) {
            var rand = ((Math.random() * (8)) << 0) + 0;
            
            direction = this.stage.getActor(this.x + this.xmoves[rand], this.y + this.ymoves[rand]);
            if(!(direction instanceof Box || direction instanceof Wall || direction instanceof Player)) {
                this.move(this, this.xmoves[rand], this.ymoves[rand]);
                check = true;
            }
        }
    }
}

Monster.prototype.move = function(other, changex, changey) {
    var newMonster = this.stage.getActor(this.x + changex, this.y + changey);
    if (!(other == this)) {
        return false;
    }
    else {
        if (newMonster == null || newMonster.move(this, changex, changey)) {
            this.stage.removeActor(this);
            this.x += changex;
            this.y += changey;
            this.stage.addActor(this);
            this.stage.setImage(this.x, this.y, this.stage.monsterImageSrc);
            return true;
        } else {
            return false;
        }
    }
}