
<head>
<meta charset="utf-8">
<meta HTTP-EQUIV="EXPIRES" CONTENT="-1">
<title>Computing Science 309 Warehouse Wars</title>
<link rel="stylesheet" type="text/css" href="style.css">
<script language="javascript" src="jquery-3.1.1.min.js">
</script>
<script language="javascript" src="ww.js">
</script>

<script>
stage = null;
// SOME GLUE CODE CONNECTING THIS PAGE TO THE STAGE
var timeLabel = document.getElementById("timeElapsed");
var msgBox = document.getElementById("msgBox");
interval = null;

function setupGame() {
    stage = new Stage(20, 20, "stage");
    stage.initialize();
}

function startGame() {
    // YOUR CODE GOES HERE
    interval = setInterval(function() {
        stage.step();
    }, 1000);
}

function pauseGame() {
    if(interval == null) {
        startGame();
        stage.controls(stage.player,true);
    }else {
        clearInterval(interval);
        stage.controls(stage.player,false);
        interval = null;
    }
    // YOUR CODE GOES HERE
}

$(document).ready(function() {
    $("#btnsignin").click(function() {
        
        var check = 'login';
        var user = $("#user").val();
        var pass = $("#password").val();
        
        var request = $.ajax({
            url: "api/api.php",
            method: "PULL",
            data: JSON.stringify({
                user: user,
                pass: pass,
                check: check
            }),
        });
        
        request.done(function(msg) {
            var c = JSON.parse(msg);
            
            if(c == "please try again") {
                alert("Invalid username/password");
            }
            else {
                if(c == "new") {
                    document.getElementById("ask").style = "display:block;";
                    document.getElementById("highscorediv").style = "display:none";
                    document.getElementById("puser").value = user;
                    var table = document.getElementById("hightable");
                    table.innerHTML = "";
                    
                    document.getElementById("logind").style = "display:none;";
                }
                else {
                    document.getElementById("welcome").style = "display:block;";
                    document.getElementById("highscorediv").style = "display:none";
                    var table = document.getElementById("hightable");
                    table.innerHTML = "";
                    document.getElementById("logind").style = "display:none;";
                }
            }
            
        });
        
        request.fail(function(jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
    });
    
    $("#btnregister").click(function() {
        
        document.getElementById("logind").style = "display:none;";
        document.getElementById("registerd").style = "display:block;";
    });
    
    $("#btnsignup").click(function() {
        var check = 'register';
        var ruser = $("#ruser").val();
        var rpass = $("#rpassword").val();
        
        
        if(ruser == "" || rpass == "") {
            alert("Please fill in all the information");
        }
        else {
            
            var request = $.ajax({
                url: "api/api.php",
                method: "PUT",
                data: JSON.stringify({
                    ruser: ruser,
                    rpass: rpass,
                    check: check
                }),
            });
            
            request.done(function(msg) {
                var k = JSON.parse(msg);
                if(k == "different username") {
                    alert("Username already exists");
                }
                else {
                    document.getElementById("logind").style = "display:block;";
                    document.getElementById("registerd").style = "display:none;";
                }
            });
            
            request.fail(function(jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
        }
    });
    
    $('#btcprofile').click(function() {
        document.getElementById("welcome").style = "display:none;";
        document.getElementById("ask").style = "display:block;";
        document.getElementById("btprofile");
        
    });
    $('#btprofile').click(function() {
        
        var check = 'profile';
        var puser = $("#puser").val();
        var ppassword = $("#ppassword").val();
        var pname = $("#pname").val();
        var pemail = $("#pemail").val();
        var org = $("#user").val();
        
        if(puser == "" || ppassword == "" || pname == "" || pemail == "") {
            alert("Please fill in all the information");
        }
        else if(org != puser) {
            alert("User not same");
            
        }
        else {
            var request = $.ajax({
                url: "api/api.php",
                method: "POST",
                data: JSON.stringify({
                    puser: puser,
                    ppassword: ppassword,
                    pname: pname,
                    pemail: pemail,
                    check: check
                }),
            });
            
            request.done(function(msg) {
                var y = JSON.parse(msg);
                if(y == "password does not match") {
                    alert("Password does not match");
                }
                else {
                    document.getElementById("ask").style = "display:none;";
                    document.getElementById("welcome").style = "display:block;";
                    document.getElementById("highscorediv").style = "display:none";
                }
                
            });
            
            request.fail(function(jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });
        }
    });
    
    
    $('#btgexit').click(function() {
        document.getElementById("tr").style = "display:none;";
        document.getElementById("my").style = "display:block;";
        clearInterval(interval);
        interval = null;
        document.getElementById("welcome").style = "display:block;";
        
    });
    $('#btntback').click(function() {
        document.getElementById("registerd").style = "display:none;";
        document.getElementById("logind").style = "display:block;";
        
    });
    
    $('#btgsignout').click(function() {
        clearInterval(interval);
        interval = null;
        stage=null;
        document.getElementById("tr").style = "display:none;";
        document.getElementById("my").style = "display:block;";
        document.getElementById("logind").style = "display:block;";
        document.getElementById("highscorediv").style = "display:block";
        getScorenew();
        
    });
    
    $('#btsignout').click(function() {
        clearInterval(interval);
        interval = null;
        stage=null;
        document.getElementById("welcome").style = "display:none;";
        document.getElementById("highscorediv").style = "display:block";
        getScorenew();
        document.getElementById("logind").style = "display:block;";
        
    });
    
    $('#btdelete').click(function() {
        var check="delete";
        var user = $("#user").val();
        var request = $.ajax({
            url: "api/api.php",
            method: "DELETE",
            data: JSON.stringify({
                user: user,
                check : check
            }),
        });
        
        request.done(function(msg) {
            alert("Account deleted!");
            document.getElementById("welcome").style = "display:none;";
            document.getElementById("logind").style = "display:block;";
            document.getElementById("highscorediv").style = "display:block";
            getScorenew();

        });
        
        request.fail(function(jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
        
    });

    $("#btgameplay").click(function() {
        document.getElementById("my").style = "display:none;";
        document.getElementById("welcome").style = "display:none;";
        document.getElementById("tr").style = "display:block;";
        document.getElementById("highscorediv").style = "display:block";
        setupGame();
        startGame();
        deanimate();
    });
});


function deanimate() {
    
    var images = document.getElementsByTagName("img");
    for (i = 0; i < images.length; i++) {
        images[i].className = "";
    }
}
// YOUR CODE GOES HERE

function getScorenew() {
    var table = document.getElementById("hightable");
    table.innerHTML = "";
    gethighscore();
}
function gethighscore() {
    var check = "gethigh";
    var request = $.ajax({
        url: "api/api.php",
        method: "PULL",
        data: JSON.stringify({
            check: check
        }),
    });
    
    
    request.done(function(msg) {
        var arr = JSON.parse(msg);
        var table = document.getElementById("hightable");
        arr.forEach(function (obj) {
            
            var row = table.insertRow(0);
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            cell1.innerHTML = obj.user;
            cell2.innerHTML = obj.highscore;
        });
        var row = table.insertRow(0);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        cell1.innerHTML =  "Username";
        cell2.innerHTML = "Score";
        
    });
    
    request.fail(function(jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
    
}
$(function() {
    gethighscore();
    
});
</script>
<style>
/* style parts of the legend, the controls and the elements in the stage appropriately */

</style>
</head>

<div id="my">
<body bgcolor="ffffff">
<center>
<div style="display:block;" id="logind">
<h1 style="color:red;">Warehouse Wars</h1>

<h2>Login</h2>
<form>
<p class="userpass">
<label for="user">User: </label>
<input type="text" id="user"></input>
</p>
<p class="userpass">
<label for="password">Password: </label>
<input type="password" id="password"></input>
</p>

</form>
<button id="btnsignin" type="submit" class="button">Login</button>
<button id="btnregister" type="submit" class="button">Register</button>
<!-- removed onclick="return" -->

</div>

<div style="display:none;" id="welcome">
<h1>Welcome</h1>
<button id="btgameplay" type="submit" class="button">Play Game</button>
<button id="btcprofile" type="submit" class="button">Edit Profile</button>
<button id="btdelete" type="submit" class="button">Delete Profile</button>
<button id="btsignout" type="submit" class="button">Sign-out</button>

</div>
<div style="display:none;" id="registerd">

<h1>Register</h1>
<form>
<p>
<label for="ruser">User: </label>
<input type="text" id="ruser"></input>
</p>
<p>
<label for="rpassword">Password: </label>
<input type="password" id="rpassword"></input>
</p>
</form>
<button id="btntback" type="submit" class="button">Back</button>
<button id="btnsignup" type="submit" class="button">Signup</button>

</div>
<div style="display:block;" id="highscorediv">
<h2>Top Highscores</h2>
<table style="center" id="hightable">
<tr>
<td></td>
<td> </td>
</tr>
</table>
</div>
<div style="display:none;" id="ask">
<form>
<h1>Profile</h1>
<p> <label for="puser">User </label> <input type="text" id="puser"</input> </p>
<p> <label for="ppassword">Password </label><input type="password" id="ppassword"></input> </p>
<p> <label for="pname">Name </label><input type="text" id="pname"></input> </p>
<p> <label for="pemail">Email </label><input type="email" id="pemail"></input> </p>
</form>
<button id="btprofile" type="submit" class="button">Submit</button>


</div>
</center>
</body>
</div>

<body bgcolor="ffffff">
<center>
<div style="display:none;" id="tr">
<table>
<tr>
<td>
<div id="msgBox">
</div>
</td>
<td>
<div id="stage"> </div>
</td>
<td>
<center>

<h2>Score</h2>
<form name="gameParams" id="gameParams" method="post" action="">
<label for="timeElapsed"><span>Time:</span>
<input type="text" value="0" name="timeElapsed" id="timeElapsed" />
</label>
</form>

<h2>Options</h2>
<button id="btpause" type="submit" onclick="return pauseGame();" class="button">Pause</button>
<button id="btgexit" type="submit" class="button">Exit</button>
<button id="btgsignout" type="submit" class="button">Sign-out</button>

<h2>Legend</h2>
<table class="legend">
<tr>
</td>
<td> <img src="icons/blank.gif" id="blankImage" border="1" /> </td>
<td> <img src="icons/emblem-package-2-24.png" id="boxImage" border="1" /> </td>
<td> <img src="icons/face-cool-24.png" id="playerImage" border="1" /> </td>
<td> <img src="icons/face-devil-grin-24.png" id="monsterImage" border="1" /> </td>
<td> <img src="icons/wall.jpeg" id="wallImage" border="1" /> </td>
</tr>
</table>

<h2>Controls</h2>
<table class="controls">
<tr>
<td><img src="icons/north_west.svg" height="45" width="45" onclick="stage.player.move(stage.player, -1, -1)" /></td>
<td><img src="icons/north.svg" height="45" width="45" onclick="stage.player.move(stage.player, 0, -1)"/></td>
<td><img src="icons/north_east.svg" height="45" width="45" onclick="stage.player.move(stage.player, 1, -1)"/></td>
</tr>
<tr>
<td><img src="icons/west.svg" height="45" width="45" onclick="stage.player.move(stage.player, -1, 0)"/></td>
<td>&nbsp;</td>
<td><img src="icons/east.svg" height="45" width="45" onclick="stage.player.move(stage.player, 1, 0)"/></td>
</tr>
<tr>
<td><img src="icons/south_west.svg" height="45" width="45" onclick="stage.player.move(stage.player, -1, 1)"/></td>
<td><img src="icons/south.svg" height="45" width="45" onclick="stage.player.move(stage.player, 0, 1)"/></td>
<td><img src="icons/south_east.svg" height="45" width="45" onclick="stage.player.move(stage.player, 1, 1)"/></td>
</tr>
</table>

</center>
</td>
</tr>
</table>
</div>
</center>
</body>
</html>
