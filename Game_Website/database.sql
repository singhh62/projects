drop table uinfo;
drop table users;

create table users ( 
    username varchar(20) primary key, 
    password varchar(20),
    times INTEGER
);

create table uinfo(
	username varchar(20) references users,
	name varchar(20),
	email varchar(20),
	password varchar(20),
	highscore INTEGER
);
