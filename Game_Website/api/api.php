
<?php
// get the HTTP method, path and body of the request
$method = $_SERVER['REQUEST_METHOD'];
$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
$input = json_decode(file_get_contents('php://input'),true);
$dbconn = pg_connect("host=mcsdb.utm.utoronto.ca dbname=singhh62_309 user=singhh62 password=19735");

if(!$dbconn){
	echo "Error Connecting to database, try again laster";
}

$check = $input['check'];
if ($method == "PULL") {
	if($check == "login") {
		$loginQuery = pg_prepare($dbconn, "q1", "SELECT times FROM users WHERE username=$1 and password=$2");
		$loginQuery = pg_execute($dbconn, "q1", array($input['user'], $input['pass']));
		$checkArray = pg_fetch_array($loginQuery);
		if(empty($checkArray)){
			echo json_encode("please try again");
		}
		else{
			if($checkArray['times'] != 0) {
				echo json_encode("been");
				
			} else {
				echo json_encode("new");
			}
		}
	}
	else if($check == "gethigh"){
		$top = pg_prepare($dbconn, "q10", "SELECT highscore, username FROM uinfo ORDER BY highscore ASC LIMIT 10");
		$top = pg_execute($dbconn, "q10", array());
		while($all = pg_fetch_array($top)) {
			$name[] = array("user" => $all["username"], "highscore" => $all["highscore"]);
		}
		echo json_encode($name);
	}
	
}
else if($method == "PUT") {
	if($check == "register") {
		$checkQuery = pg_prepare($dbconn, "q2", "SELECT username, password FROM users WHERE username=$1");
		$checkQuery = pg_execute($dbconn, "q2", array($input['ruser']));
		$result = pg_fetch_array($checkQuery);
		
		if(empty($result)){
			
			$query = pg_prepare($dbconn, "q3", "INSERT INTO users VALUES($1, $2, $3)");
			$query = pg_execute($dbconn, "q3", array($input['ruser'], $input['rpass'], 0));
			echo json_encode("inserted");
		}
		else{
			echo json_encode("different username");
		}
	}
	else if($check == 'highscoree'){
		$highscore = pg_prepare($dbconn, "q6", "SELECT highscore FROM uinfo WHERE username=$1");
		$highscore = pg_execute($dbconn, "q6", array($input['user']));
		$highCheck = pg_fetch_array($highscore);
		if($highCheck['highscore'] < $input['highscore']){
			$highInsert = pg_prepare($dbconn, "q7", "UPDATE uinfo set highscore=$1 where username=$2");
			$highInsert = pg_execute($dbconn, "q7", array($input['highscore'], $input['user']));
		}
	}
}
else if($method == "POST"){
	if($check == "profile"){
		$checkPass = pg_prepare($dbconn, "q4", "SELECT times from users where password=$1 and username=$2");
		$checkPass = pg_execute($dbconn, "q4", array($input['ppassword'], $input['puser']));
		$passResult = pg_fetch_array($checkPass);
		if(!empty($passResult)){
			if($passResult['times'] == 0) {
				$profQuery = pg_prepare($dbconn, "q5", "INSERT INTO uinfo(username, name, email, password, highscore) VALUES($1, $2, $3, $4, 0)");
				$profQuery = pg_execute($dbconn, "q5", array($input['puser'], $input['pname'], $input['pemail'], $input['ppassword']));
				$changeTimes = pg_prepare($dbconn, "q8", "UPDATE users set times=1 where username=$1");
				$changeTimes = pg_execute($dbconn, "q8", array($input['puser']));
			}
			else {
				$changeInfo = pg_prepare($dbconn, "q9", "UPDATE uinfo set name=$1, email=$2 where username=$3");
				$changeInfo = pg_execute($dbconn, "q9", array( $input['pname'], $input['pemail'], $input['puser']));
			}
			echo json_encode("inserted");
		}
		else{
			echo json_encode("password does not match");
		}
	}
}
else if($method == "DELETE"){
	if($check == "delete"){

		$delete = pg_prepare($dbconn, "q12", "DELETE FROM UINFO WHERE username=$1");
		$delete = pg_execute($dbconn, "q12", array($input['user']));
		$delete2 = pg_prepare($dbconn, "q13", "DELETE FROM USERS WHERE username=$1");
		$delete2 = pg_execute($dbconn, "q13", array($input['user']));
	}
}
?>
