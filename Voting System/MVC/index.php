<?php
	require_once "model/Database.php";
	session_save_path("sess");

	session_start();

	$_SESSION['Database'] = new Database(); //calls the model
	$errors=array();
	$view="";

	/* controller code */
	if(!isset($_SESSION['state'])){  //RIGHT IN START GO TO login state
		$_SESSION['state']='login';
	}

	switch($_SESSION['state']){
		case "login":
			$view="login.php"; //change view to login

			if (isset($_REQUEST['login'])){  //if login button is clicked
					if($_SESSION['Database']->checkUser($_REQUEST['username'], $_REQUEST['password'])){  //checks if user in database
						$_SESSION['user'] = $_REQUEST['username'];
						$_SESSION['state']="voted";  //change state to diffrent screen
						do();
						$view = "vote.php"; //change the looks
					}
					else {
						echo '<script language="javascript">'; //how to do alert in php
						echo 'alert("Invalid user/pass, please try again")';
						echo '</script>';
						$_SESSION['state'] = 'login';
						$view = "login.php";
					}
			}
			break;

	case "voted":  //diffrent state
		$view = "vote.php";

		if(isset($_REQUEST['logout'])){  //if they click logout, u have to destroy
			session_destroy();
			$view="login.php";
		}
		else if(isset($_REQUEST['refresh'])){
					do();
					$view="vote.php";
					$_SESSION['state'] ="voted";
	  }
		else if(isset($_REQUEST['about'])){
					$view="about.php";
					$_SESSION['state'] ="about";
	  }
		else {
			if (isset($_REQUEST['role'])) {  //if they clicked role
				$answer = $_REQUEST['role']; //set answer to that role
				if($_SESSION['Database']->checkIfVoted($_SESSION['user'])) { //if they voted already
					if($answer == "yes") { //change
						$_SESSION['Database']->changeVoted($_SESSION['user'], 1);
						do();
						$_SESSION['state'] = 'voted';
						$view = "vote.php";
					}
					else {
						$_SESSION['Database']->changeVoted($_SESSION['user'], 0);
						do();
						$_SESSION['state'] = 'voted';
						$view = "vote.php";
					}
				}
				else {
					if($answer == "yes") { //else insert
						$_SESSION['Database']->insertVote($_SESSION['user'], 1);
						do();
						$_SESSION['state'] = 'voted';
						$view = "vote.php";
					}
					else {
						$_SESSION['Database']->insertVote($_SESSION['user'], 0);
						do();
						$_SESSION['state'] = 'voted';
						$view = "vote.php";
					}
				}
			}
	}
		break;

		case "about": //other about view
			$view = "about.php";
			if(isset($_REQUEST['logout'])){
				session_destroy();
				$view="login.php";
			}
			else if(isset($_REQUEST['vote'])){
						do();
						$view="vote.php";
						$_SESSION['state'] ="voted";
		  }
    }
		function do() {  //function to keep updating vote
			$_SESSION['vote0'] = $_SESSION['Database']->castVote0();
			$_SESSION['vote1'] = $_SESSION['Database']->castVote1();
		}
require_once "view/$view";
?>
