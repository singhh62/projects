<head>
<meta charset="utf-8">
<meta HTTP-EQUIV="EXPIRES" CONTENT="-1">
<title>Voting 2017</title>
<link rel="stylesheet" type="text/css" href="style.css">
<script language="javascript" src="jquery-3.1.1.min.js">
</script>

<script>
$(document).ready(function() {
    $("#btnsignin").click(function() {

        var check = 'login';
        var user = $("#username").val(); //gets username value
        var pass = $("#password").val();

        var request = $.ajax({
            url: "api/api.php",
            method: "POST",
            data: JSON.stringify({
                user: user,
                pass: pass,
                check: check,
            }),
        });

        request.done(function(msg) {
            var c = JSON.parse(msg);
            if(c == "please try again") {
                alert("Invalid username/password");
            }
            else {
                $("#logind").hide();
                $("#vote").show();
                $("#header").show();
                getScorenew();
            }

        });
        request.fail(function(jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
    });

    $("#btnvote").click(function() {

        var check = 'vote';
        var user = $("#username").val(); //Get the original username

        if(document.getElementById("roley").checked == true) {  //HOW TO CHECK WITH RADIO IS CLICKED
          var answer = "yes";
        }
        else {
          var answer = "no";
        }
        var request = $.ajax({
            url: "api/api.php",
            method: "POST",
            data: JSON.stringify({
                answer: answer,
                check: check,
                user: user,
            }),
        });

        request.done(function(msg) {
            $("#vote").show();
            document.getElementById('roley').checked = false; //unclicks the option
            document.getElementById('rolen').checked = false; //unclicks the option
            getScorenew(); //updates table

        });
        request.fail(function(jqXHR, textStatus) {
            document.getElementById('roley').checked = false; //unclicks the option
            document.getElementById('rolen').checked = false; //unclicks the option
            alert("Request failed: " + textStatus);
        });
    });

    $("#refresh").click(function() {
      var table = document.getElementById("votetable"); //When clicked, clear table and re populate
      table.innerHTML = "";
      getvote();
    });

    $("#logout").click(function() {
      $("#vote").hide();
      $("#header").hide();
      $("#logind").show();
    });

  }); //CLOSES DOCUMENT READY

  function getScorenew() {
      var table = document.getElementById("votetable");
      table.innerHTML = "";
      getvote();
  }
  function getvote() {
      var check = "getvote";
      var request = $.ajax({
          url: "api/api.php",
          method: "POST",
          data: JSON.stringify({
              check: check
          }),
      });

      request.done(function(msg) {
          var arr = JSON.parse(msg);
          var table = document.getElementById("votetable");
          arr.forEach(function (obj) { //Go through the array that

              var row = table.insertRow(0);
              var cell1 = row.insertCell(0);
              var cell2 = row.insertCell(1);
              cell1.innerHTML = obj.user;
              cell2.innerHTML = obj.highscore;
          });

      });

      request.fail(function(jqXHR, textStatus) {
          alert("Request failed: " + textStatus);
      });

  }
  $(function() {  //TO just refresh the table
    var table = document.getElementById("votetable");
    table.innerHTML = "";
    window.setInterval(getScorenew, 5000);  //Refresh every 5 seconds
  });
</script>

</head>

<div id="my">
<body bgcolor="ffffff">
<center>
<div style="display:block;" id="logind">
<h1 style="color:red;">Voting 2017</h1>

<h3 align="center">Login</h3>
<hr>
<form >
    <p class="userpass"> <label for="username">Username:</label> <input type="text" id="username"></input> </p>
    <p class="userpass"> <label for="password">Password:</label> <input type="password" id="password"></input> </p>
</form>

<button id="btnsignin" type="submit" class="button">Login</button>  <!-- AJAX GOTTA USE BUTTONS!! -->

</div>
<div style="display:none;" id="header">
  <header><h1 align="center">Class Vote</h1></header>
  <!-- HEADINGS ON TOP, LOOK AT CSS TO SEE HOW IT HOVERS -->
  <div class="categories" align="center">
      <a id="logout">Logout</a>
      </div>
  <hr>

</div>

<div style="display:none;" id="vote">
<form align="center">
		<p> <label for="role"><p style="color:red"><?php echo $_SESSION['user'] ?> preference for extension</p></label>
			<input type="radio" id="roley" value="yes">No DURR!!</input>
		</br>
			<input type="radio" id="rolen" value="no">I'm a nerd</input>
		</p>
</form>
  <button id="btnvote" type="submit" class="button">Submit vote!</button>
</br>
<hr>

<div style="display:block;" id="votediv">
<h2 align="center">Current Votes</h2>
<table align="center" border="1" id="votetable">
  <!-- //TABLE THAT HAS TWO COLUMNS SO IT CAN AUTO FILL -->
  <tr>
  <td></td>
  <td> </td>
  </tr>
</table>
</div>
</div>
</div>
</html>
