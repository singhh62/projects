<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="refresh" content="30; url=index.php?refresh=true">
		<link rel="stylesheet" type="text/css" href="style.css" />
		<style>
			span {
				background-color:green; 
				display:block; 
				width:100px; 
				text-decoration:none; 
				padding:20px; 
				color:white; 
				text-align:center;
			}
		</style>
		<title>iGetIt</title>
	</head>
	<body>
		<header><h1>iGetIt (instructor)</h1></header>
		<nav>
  			<ul>
                        <li> <a href="index.php?class=true">Class</a>
                        <li> <a href="index.php?profile=true">Profile</a>
                        <li> <a href="index.php?logout=true">Logout</a>
                        </ul>
		</nav>
		<main>
			<h1>Class</h1>
			<form>
				<fieldset style="width: 100%">
					<legend align="center"> <?php echo $_SESSION['inclass'];?></legend>
					<span style="background-color:#1EAAC2; width:  <?php echo $_SESSION['Gcount']; ?>%;" >I Get It <?php echo ": " . $_SESSION['Gcount'] . "%"; ?></span> 
					<span style="background-color:#EA4E4E; width:  <?php echo $_SESSION['Bcount']; ?>%;"  >I Don't Get It <?php echo ": " . $_SESSION['Bcount'] . "%"; ?></span>
				</fieldset>
			</form>
		</main>
		<footer>
		</footer>
	</body>
</html>

