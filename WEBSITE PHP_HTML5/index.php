<?php
	require_once "model/Database.php";

	session_save_path("sess");

	session_start();

	$_SESSION['Database'] = new Database();
	$errors=array();
	$view="";

	/* controller code */
	if(!isset($_SESSION['state'])){
		$_SESSION['state']='login';
	}
	
	switch($_SESSION['state']){
		case "login":
			$view="login.php";
			if(isset($_REQUEST["reg"])){
				$_SESSION['state'] = "signup";
				$view = "signup.php";
			}	

			if(empty($user)){
				$errors[] =  "user is required";
			}
			if(empty($password)){
				$errors[]='password is required';
			}
			
			if(isset($_REQUEST['login'])) {
				$_SESSION['mainUser'] = $_REQUEST['user'];

				if($_SESSION['Database']->userExists($_REQUEST['user'], $_REQUEST['password'])){
					if($_SESSION['Database']->times($_REQUEST['user'])) {
						$_SESSION['state'] = "profile";
						$view = "profile.php";
					}
					else {
						if($_SESSION['Database']->getInsRole($_REQUEST['user'])) {
						
							$_SESSION['instUser'] = $_REQUEST['user'];
							$_SESSION['Database']->getInstructorClass($_SESSION['instUser']);
							
							$_SESSION['state'] = "instructor_create";
							$view = "instructor_createclass.php";
						}
						else {
							$_SESSION['stuUser'] = $_REQUEST['user'];
							$_SESSION['Database']->getStudClass();
							
							$_SESSION['state'] = "student_join";
							$view = "student_joinclass.php";
						}
					}
				}
				else {
					echo "Username and Password don't match";
				
				}
			}
			break;

		case "profile":
			// the view we display by default
			$view="profile.php";
			
			if(isset($_REQUEST['logout'])){
				session_destroy();
				$view="login.php";
			}
			
			if(isset($_REQUEST['class'])){
				
				if($_SESSION['Database']->getInsRole($_SESSION['mainUser']) && $_SESSION['Database']->Usertimes($_SESSION['mainUser'])){
					$_SESSION['Database']->getInstructorClass($_SESSION['instUser']); 
					$_SESSION['state'] = "instructor_create";
					$view="instructor_createclass.php";
				}
				else if(!($_SESSION['Database']->getInsRole($_SESSION['mainUser']))  && $_SESSION['Database']->Usertimes($_SESSION['mainUser'])){
					$_SESSION['Database']->getStudClass();
					$_SESSION['state'] = "student_join";
					$view="student_joinclass.php";
				}
			}
	
			if(empty($_REQUEST['puser'])){
				$errors[]='user is required';
			}
			
			if(empty($_REQUEST['ppassword'])){
				$errors[]='password is required';
			}
			if(empty($_REQUEST['pfirstName'])){
				$errors[]='first name is required';
			}
			if(empty($_REQUEST['plastName'])){
				$errors[]='last name is required';
			}
			if(empty($_REQUEST['pemail'])){
				$errors[]='email is required';
			}
			
			else {
				if($_SESSION['Database']->userExists($_REQUEST['puser'], $_REQUEST['ppassword'])) {
					
					if (isset($_REQUEST['role'])) {
					
						if($_SESSION['Database']->emailExists($_REQUEST['pemail'])) {
							
							$_SESSION['Database']->updateI($_REQUEST['puser'], $_REQUEST['pfirstName'], $_REQUEST['plastName'],$_REQUEST['role'], $_REQUEST['pemail']); 
				
						
							$answer = $_REQUEST['role'];
							if($answer == "instructor") {
								$_SESSION['instUser'] = $_REQUEST['puser'];
								$_SESSION['Database']->getInstructorClass($_REQUEST['puser']);
								
							
								$_SESSION['state'] = "instructor_create";
								$view = "instructor_createclass.php";
				
							}
		
							else if ($answer == "student"){
								$_SESSION['stuUser'] = $_REQUEST['puser'];
							
								$_SESSION['Database']->getStudClass();
								$view = "student_joinclass.php";
							}
						}
						else {
							$errors[] =  "Email already exists, try again";
							
						}
						}
						
						else {
							$errors[] =   "Pick a role and try again";
						}
						
				}
				else {
				$errors[] =  "Please make sure user and password match!";
				}
			}
		
			break;
			
		case "instructor_create":
			$view = "instructor_createclass.php";
			if(isset($_REQUEST['logout'])){
				session_destroy();
				$view="login.php";
			}
			if(isset($_REQUEST['profile'])){
				$view="profile.php";	
			}
			
			if(empty($_REQUEST['cclass'])){
				$errors[]='class name is required';
			}
			if(empty($_REQUEST['code'])){
				$errors[]='code is required';
			}
			
			$_SESSION['Database']->getInstructorClass($_SESSION['instUser']);
			
			if(isset($_REQUEST["create"])) {
			
				if($_SESSION['Database']->classCheck($_REQUEST['cclass'])){
				
					$_SESSION['state'] = "instructor_create";
					$view = "instructor_createclass.php";
				}
				else{
					$_SESSION['Database']->addTaught($_SESSION['instUser'], $_REQUEST['cclass'], $_REQUEST['code']);
					$_SESSION['Database']->getInstructorClass($_SESSION['instUser']); 
							
					$_SESSION['state'] = "instructor_create";
					$view = "instructor_createclass.php";
				}
			}
			if(isset($_REQUEST['Enter'])){
				$_SESSION['inclass'] = $_POST['theClass'];
				
				if($_SESSION['Database']->checkZero($_POST['theClass'])){
					$_SESSION['state'] = "instruct_current";
					$view = "instructor_currentclass.php";
				}
			}
		
			break;
		
		case "instruct_current":
		
			$view = "instructor_currentclass.php";

			if(isset($_REQUEST['logout'])){
				session_destroy();
				$view="login.php";
			}
			
			if(isset($_REQUEST['profile'])){
				$_SESSION['state'] = "profile";
				$view="profile.php";	
			}
			
			if(isset($_REQUEST['class'])){
				$_SESSION['Database']->getInstructorClass($_SESSION['instUser']); 
				$_SESSION['state'] = "instructor_create";
				$view="instructor_createclass.php";				
			}
			
			if(isset($_REQUEST['refresh'])){
				$_SESSION['state'] = "instruct_current";
				$view = "instructor_currentclass.php";
			
			}
			
			break;
		
		case "student_join":
			$view = "student_joinclass.php";
			
			if(isset($_REQUEST['logout'])){
				session_destroy();
				$view="login.php";
			}
			if(isset($_REQUEST['profile'])){
				$view="profile.php";	
			}
			
			if(empty($_REQUEST['code'])){
				$errors[]='code is required';
			}
			
			if(isset($_REQUEST['sub'])) {

				if($_SESSION['Database']->checkCode($_POST['joiner'], $_POST['code'])) {
					$_SESSION['className'] = $_POST['joiner'];

					$_SESSION['state']="student_current";
					$view="student_currentclass.php";
				}
				else {
					$errors[] =  "Please enter the right code";
					$_SESSION['Database']->getStudClass(); 
					$view = "student_joinclass.php";
				}
			}
				
			
			break;
		
		case "student_current":
			$view = "student_currentclass.php";
			if(isset($_REQUEST['logout'])){
				session_destroy();
				$view="login.php";
			}
			
			if(isset($_REQUEST['profile'])){
				$view="profile.php";	
			}
			
			if(isset($_REQUEST['class'])){
			
				$_SESSION['Database']->getStudClass();
				$_SESSION['state'] = "student_join";
				$view="student_joinclass.php";	
			}
			
			if(isset($_REQUEST['getit'])){
			
				$_SESSION['Database']->iGetitExists($_SESSION['mainUser'],  $_SESSION['className'],  'getit', 'yes');	
			} 
			
			if(isset($_REQUEST['dgetit'])){
				
				$_SESSION['Database']->iGetitExists($_SESSION['mainUser'],  $_SESSION['className'],  'dgetit', 'no');

			}
			
			
			break;
			
		case "signup":
			$view='signup.php';
			
			if(isset($_REQUEST['logout'])){
				session_destroy();
				$view="login.php";
			}
			if(isset($_REQUEST["back"])){
				$_SESSION['state'] = "login";
				$view = "login.php";
			}	
			if(empty($_REQUEST['user'])){
				$errors[]='user is required';
			}
			if(empty($_REQUEST['password'])){
				$errors[]='password is required';
			}
			else {
				if($_SESSION['Database']->newUserVal($_REQUEST['user'], $_REQUEST['password'])){
				
					$view = "login.php";
				}
				else {
					echo "Invalid user attempt";
				
				}
			}
			break;
			
		}
	require_once "view/view_lib.php";
	require_once "view/$view";
?>
