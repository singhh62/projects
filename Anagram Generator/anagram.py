#**********************************************************
# Assignment2:anagram.py
# UTOR user_name:singhh62
# First Name:Hardeep
# Last Name:Singh
# Student #1001849556
#
#
#
# Honour Code: I pledge that this program represents my own
# Page 3 of 4CSC148H Winter 2015 Assignment #2
# program code and that I have coded on my own. I received
# help from no one in designing and debugging my program.
# I have also read the plagiarism section in the course info sheet
# of CSC 148 and understand the consequences.
#*********************************************************

from letterManager import *
import doctest

def list_generator(file):
  """Creates a list of words from the file of words entered"""
  
  file = open(file,'r')
  dict_list = []
  for i in file:
    dict_list.append(i.strip())

  file.close()
  return dict_list


class AnagramSolver:
  def __init__ (self, listOfWords):
    self.dict = listOfWords


  def fractedDict(self,s):
    """Gets a small dictionary of words from the original dictionary, the words are similar to the phrase
    entered

    >>> object.fractedDict('office')
    ['fief', 'fife', 'foci', 'foe', 'ice', 'off', 'office']
    """
    fracted_dict = []
    string = LetterManager(s)
    for word in self.dict:
      temp = string.Subtract(LetterManager(word))
      if temp != None:
        for count in range(len(temp.counts)):
          if (temp.counts[count] == string.counts[count]) and (word not in fracted_dict):
            fracted_dict.append(word)

    return fracted_dict
  

  def stringExists (self, str, str2):
    """If string is in other string return true, if not false

    >>> object.stringExists('office','office key')
    True
    """
    return(LetterManager(str2).Subtract(LetterManager(str)) != None)
    

  def removeString (self, str, str2):
    """Removes word from a string, it could also be letters

    >>> object.removeString('office','office key')
    'eky'
    """
    new = LetterManager(str2).Subtract(LetterManager(str))
    return new.__str__()
  
    
  def generateAnagrams (self, s, max):
    """ Takes in a phrase and returns all the possible anagrams in a list of
    list form"""
    
    anagram = []
    if (len(s) == 0 or len(s) == 1): 
      return []   #base case
    
    elif (max >= 0): #ONLY if max is the condition
      word_dict = self.fractedDict(s)
      s = s.replace(' ','')
      
      for i in word_dict:
        if self.stringExists(i, s): #If it exists in string, remove it 
          s1 = self.removeString(i, s)
          
          if (len(s1) == 0): #if all are removed 
            anagram.append([i])
            return anagram
          
          #recursion will get list back for the smaller string.
          #returns it as [["string"]] 
          #or if many matches, [[a,b,...],[c,d,...],...] 
          shortList = self.generateAnagrams(s1,max)
          
          #no match was found, ignore this "i"
          if (shortList == []):
            pass
          
          #only one match, so just add "i" to the beginning and add it to nagram list
          elif (type(shortList[0]) is not list):
            shortList.append(i)
            anagram.append(shortList)
            
          #multiple matches, so need to add "i" to beginning of every item in list
          #then add this new [[i,a,b,...],[i,c,d,...],...] list to anagram list
          else:
            new = []
            for x in shortList:
              new.append(i)
              new.extend(x)
              if max >= 1:
                if len(new) <= max:
                  anagram.append(new)
              elif max == 0:
                anagram.append(new)
              new = []
           
    elif max < 0:
      raise ValueError('Error: Max has to be greater than 0!')
                
    return anagram
  

      


