//
//  GameScene.swift
//  BrickJump
//
//  Created by Hardeep Singh on 2016-06-19.
//  Copyright (c) 2016 DeepApps. All rights reserved.
//

import SpriteKit

    //category bitmasks
    let brickMask: UInt32        = 0x1 << 0 // 1 for brick
    let floorMask: UInt32        = 0x1 << 1 // 2 for floor
    let wallMask: UInt32         = 0x1 << 2 // 4 for wall
    let gameOverWallMask: UInt32 = 0x1 << 3 // 8 for gameOverWall 




class GameScene: SKScene, SKPhysicsContactDelegate {
    
    //counts the number of touches when jumping
    var positionCounter: Int = 0
    
    
    
    /*Set up scene*/
    override func didMove(to view: SKView) {
        //set infinite action for wall
        self.physicsWorld.contactDelegate = self
    
        let wait = SKAction.wait(forDuration: 2.0)
        let spawn = SKAction.run(spawnWall)
        let spawning = SKAction.sequence([spawn, wait])
        
        run(SKAction.repeatForever(spawning))
    }
    
    
    /* Called when a touch begins */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        jumpWhileOnFloor()
    }
    
    
    //Executes a jump only when brick is close enough to the ground
    func jumpWhileOnFloor() {
        
        let brick: SKSpriteNode = self.childNode(withName: "brick") as! SKSpriteNode
        let floor: SKSpriteNode = self.childNode(withName: "floor") as! SKSpriteNode
        
        //find the position of the base of the brick
        var brickBasePosition = brick.position.y - (brick.size.height/2)
        //find position of the top of the floor
        var floorBasePosition = floor.position.y + (floor.size.height/2)
        
        //round to the 4th decimal place
        brickBasePosition = round(1000*brickBasePosition)/1000
        floorBasePosition = round(1000*floorBasePosition)/1000
        
        //get difference of space between floor and brick
        let diff = brickBasePosition - floorBasePosition
        
        //jump only when the brick is pretty much touching the floor
        if (diff <= 0.3){
            jump(brick)
        }
    }
    

    /*Action sequence for jump action called in jumpWhileOnFloor*/
    func jump(_ sprite: SKSpriteNode) {
        
        sprite.physicsBody?.applyImpulse(CGVector(dx: 0,dy: 400))
        let rotate = SKAction.rotate(byAngle: CGFloat(-M_PI/2), duration: 0.2)
        sprite.run(rotate)
    }

    
    
    
    /* Adds a wall to the scene */
    func spawnWall(){
        
        let wall: SKSpriteNode = SKScene(fileNamed: "Wall")!.childNode(withName: "wall") as! SKSpriteNode
        wall.physicsBody?.categoryBitMask = wallMask
        wall.physicsBody?.contactTestBitMask = brickMask
        
        let floor: SKSpriteNode = self.childNode(withName: "floor") as! SKSpriteNode
        
        wall.removeFromParent()
        
        self.addChild(wall)
        
        //chose random height for wall upon spawn
        wall.size.height = CGFloat(arc4random_uniform(200) + 50)

        //get the y position of the top of the floor
        let topOfFloor = floor.position.y + floor.size.height/2
        let wallYPosition = topOfFloor + wall.size.height/2
        let floorSide = floor.position.x + floor.size.width/2 - 20
        wall.position.x = floorSide
        wall.position.y = wallYPosition
        
        moveWall(wall)
    }
    //moves wall across screen
    func moveWall(_ wall: SKSpriteNode){
        let speed = 3.0
        let moveAction = SKAction.moveBy(x: -size.width, y: 0, duration: speed)
        wall.run(moveAction)
    }
    //contact detection
    func didBegin(_ contact: SKPhysicsContact) {
        let brick = (contact.bodyA.categoryBitMask == brickMask) ? contact.bodyA : contact.bodyB
        let other = (brick == contact.bodyA) ? contact.bodyB : contact.bodyA
        
        if(other.categoryBitMask == wallMask)
        {
            print("Hit a Wall!")
        }
        else if(other.categoryBitMask == gameOverWallMask)
        {
            print("Game Over!")
        }
    }
    //pauses game on contact with wall
    func endGame(){
        scene?.view?.isPaused = true
    }
    
    override func update(_ currentTime: TimeInterval){
        /* Called before each frame is rendered */
    }
}
