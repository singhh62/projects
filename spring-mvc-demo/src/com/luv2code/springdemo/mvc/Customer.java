package com.luv2code.springdemo.mvc;

import com.luv2code.springdemo.mvc.validation.CourseCode;

import javax.validation.constraints.*;

public class Customer {

    //-------------------------First Name-----------------------------------//
    private String firstName;

    //-------------------------Last Name-----------------------------------//
    @NotNull(message="is required")
    @Size(min=1, message="is required")
    private String lastName;

    //-------------------------Free Passes-----------------------------------//
    @NotNull(message = "is required")
    @Min(value = 0,  message = "must be between 0 and 10")
    @Max(value = 10, message = "must be between 0 and 10")
    private Long freePasses;


    //-------------------------Postal Code -----------------------------------//
    @Pattern(regexp = "^[a-zA-Z0-9]{5}", message = "only 5 chars/digits")
    private String postalCode;


    //-------------------------Course Code -----------------------------------//
    @CourseCode(value={"LUV","TOPS"}, message = "Must start with TOPS or LUV")
    private String courseCode;


    //-------------------------Getter and Setter Methods----------------------------------//
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getFreePasses() {
        return freePasses;
    }

    public void setFreePasses(Long freePasses) {
        this.freePasses = freePasses;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }
}