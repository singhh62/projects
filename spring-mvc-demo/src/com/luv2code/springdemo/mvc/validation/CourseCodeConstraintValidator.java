package com.luv2code.springdemo.mvc.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class CourseCodeConstraintValidator implements ConstraintValidator<CourseCode, String> {

   private String[] coursePrefixes;

   public void initialize(CourseCode theCourseCode) {
      coursePrefixes = theCourseCode.value();
   }

   public boolean isValid(String theCode, ConstraintValidatorContext theContext) {

       if (theCode != null) {
           //see if any prefix matches matches with code
           for(String prefix : coursePrefixes) if(theCode.startsWith(prefix)) return true;
           //never found
           return false;
       }
       else{//value is allowed to be empty
           return true;
       }
   }

}
